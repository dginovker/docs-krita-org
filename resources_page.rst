.. meta::
   :description:
        Resource Packs for Krita.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
             - Scott Petrovic
             - Raghavendra Kamath <raghu@raghukamath.com>
             - Nathan Lovato
             - Pedro Reis <pedroreis.ad@protonmail.com>
             - Agata Cacko <tamtamy.tymona@gmail.com>
             
   :license: GNU free documentation license 1.3 or later.


.. _resources_page:

#########
Resources
#########


Brush Packs
===========

.. list-table::

        * - .. figure:: /images/resource_packs/Resources-mirandaBrushes.jpg
               :target: https://drive.google.com/open?id=1hrH4xzMRwzV0SBEt2K8faqZ_YUX-AdyJ

               Ramon Miranda

          - .. figure:: /images/resource_packs/Resources-conceptBrushes.jpg
               :target: https://forum.kde.org/viewtopic.php?f=274&t=127423

               Concept art & Illustration Pack
               
          - .. figure:: /images/resource_packs/Resources-aldyBrushes.jpg
               :target: https://www.deviantart.com/al-dy/art/Aldys-Brush-Pack-for-Krita-2-3-1-196128561

               Al-dy

        * - .. figure:: /images/resource_packs/Resources-vascoBrushes.jpg
               :target: https://vascobasque.wordpress.com/2014/02/03/modular-brushset-v4/

               Vasco Basqué

          - .. figure:: /images/resource_packs/Resources-stalcryBrushes.jpg
               :target: https://www.deviantart.com/stalcry/art/Krita-Custom-Brushes-350338351

               Stalcry

          - .. figure:: /images/resource_packs/Resources-woltheraBrushes.jpg
               :target: https://forum.kde.org/viewtopic.php?f=274&t=125125

               Wolthera

        * - .. figure:: /images/resource_packs/Resources-nylnook.jpg
               :target: https://nylnook.art/en/blog/krita-brushes-pack-v2/

               Nylnook


          - .. figure:: /images/resource_packs/Resources-raghukamathBrushes.png
               :target: https://gitlab.com/raghukamath/krita-brush-presets/-/releases

               Raghukamath

          - .. figure:: /images/resource_packs/Resources-GDQuestBrushes.jpeg
               :target: https://github.com/GDquest/free-krita-brushes/releases/

               GDQuest

        * - .. figure:: /images/resource_packs/Resources-iForce73Brushes.png
               :target: https://www.deviantart.com/iforce73/art/Environments-2-0-759523252

               IForce73

          - .. figure:: /images/resource_packs/Resources-wojtrybBrushes.png
               :target: https://www.dropbox.com/s/i1rt7f0qc77nc4m/wont_teach_you_to_draw_brushpack_v5.0.zip?dl=1

               wojtryb

          - .. figure:: /images/resource_packs/Resources-rakurribrushset.png
               :target: https://github.com/Rakurri/rakurri-brush-set-for-krita

               Rakurri


         
Texture Packs
=============

.. list-table::

        * - .. figure:: /images/resource_packs/Resources-deevadTextures2.jpg
               :target: https://www.davidrevoy.com/article263/five-traditional-textures
               :width: 300

               David Revoy

External tutorials
==================

.. list-table::

        * - .. figure:: /images/resource_packs/simon_pixel_art_course.png
               :target: https://www.udemy.com/learn-to-create-pixel-art-from-zero/?couponCode=OTHER_75
               :width: 400

               Simón Sanchez' "Learn to Create Pixel Art from Zero" course on Udemy


Pre-installed Python Plugins
============================

Krita's capabilities can be enhanced by using specific plugins. This list will present the plugins that come with Krita by default, as well as other plugins made by the community. If you would like to learn more about how you can make your own plugin, visit the :ref:`krita_python_plugin_howto` area.

If you want to know more about an individual plugin, you can access the plugin's manual by going to :menuselection:`Settings --> Configure Krita...` menu, and then choosing the Python Plugin Manager Tab. There, you can click on a specific plugin, and the manual will appear in the bottom text area. 

Assign Profile Dialog:
----------------------

    Allows you to assign a profile to an image instead of converting it to that profile. The difference is that it allows only interpreting the colors by the new profile, but not change any of the values. It can be found in :menuselection:`Tools --> Assign Profile to Image...`, and will present a list of profiles for the current image's color model.

Batch Exporter
--------------

    Plugin for Game Developers and Graphic Designers.
    
    - Allows batch export of assets to multiple sizes, file types and custom paths;
    - Renames layers quickly with the smart rename tool;
    - Export all layers or only selected layers;

    By default, the plugin exports the images in an :file:`export` folder next to the Krita document, and follows the structure of your layer stack.


Channels to Layers
------------------
    Splits channels from a layer to sub-layers. To use, select the layer to convert, click on :menuselection:`Tools --> Scripts --> Channels to Layers...`, and choose options from the user interface. 

Color Space
-----------

    Allows you to select a document and convert it's colors to a new color space, like RGBA, CMYKA, L*a*b, etc.

Comics Project Management Tools 
-------------------------------

    Aims to simplify comics creation by: 

    - Giving the artist a way to organize and quickly access their pages;
    - Helping the artist(s) deal with the boring bits meta data bits of a comic project by giving a meta-data editor that gives suggestions, explanation and occasionally a dab of humor;
    - Making export set-and-forget type of affair where a single click can export to multiple formats with proper meta-data.

    Also features options to assist exporting to specific formats, like "Advanced Comic Book Format", CBZ or Epub. 

    To use this plugin, activate the "Comics Manager" Docker.

Document Tools
--------------

    Allows you to select a document and scale, crop and rotate in one action.

Export Layers
-------------

    Allows you to select a document and export its layers in an ordered and sensible manner.

Filter Manager 
--------------

    A plugin to enable you to select a document or one of its layers and quickly apply a filter.

High Pass
---------

    Performs a high pass filter on the active document.

Krita Script Starter
--------------------

    A script that helps set up the various files that Krita expects to see when it runs a script, namely:

    - The .desktop meta data file;
    - The package directory;
    - The __init__.py file;
    - The main python file for your package;
    - The Manual.html file for your documentation;

Last Documents Docker
---------------------

    Script that shows the recently opened documents as a thumbnail image. To use this, activate the "Last Documents" docker.

Mixer Slider Docker
-------------------

    Docker that allows you to choose from the gradients between two colors. 

Palette Docker
--------------

    A Docker that allows you to control palettes more easily. You can add swatches, groups and export the palette settings, or even the palette itself as a GIMP Palette or Inkscape SVG.

Photobash Images Docker
-----------------------

    Simple Krita Plugin that lists the images you have on a folder you specify, with the ability to filter by words in the path. To use, activate the "Photobash Images" docker, and set the references directory. After that, you can:

    - Filter images by words. Using multiple words like "rock marble" will show all the images that have rock OR marble in the name;
    - Scroll the pages to access more results;
    - Click on an image to create a layer, with the scale that you specify.

Python Plugin Importer
----------------------

    Imports Python plugins from zip files.

Quick Settings Docker 
---------------------

    A Docker that allows you to quickly set the opacity, flow and size from a predefined list. Now, when you have a document open, and a brush selected, select any of the entries in the "Quick Settings" docker to change them. The docker doesn’t update when changing brush size any other way, so it will always show the last selected change.

Scripter
--------

    A small Python scripting console, allows to write code in an editor and run it, with feedback related to the output of the execution. You can also debug your code using the "Debug" button. 

Ten Brushes 
-----------

    A Script to assign presets to one of ten configurable hotkeys. To use, go to :menuselection:`Tools --> Scripts --> Ten Brushes`, and a window will pop up with a preset chooser and ten boxes above it. Underneath the boxes is the hotkey the box is associated with. 

    Customize your shortcuts by editing the configurations in :menuselection:`Settings --> Configure Krita --> Keyboard Shortcuts`, and then change the "Activate Brush Preset" actions under "Ten Brushes".

Ten Scripts
-----------

    Similar to Ten Brushes, allows the assign of Python scripts to one of ten configurable hotkeys. 

User-made Python Plugins
========================

To install and manage your plugins, see the second area of the :ref:`krita_python_plugin_howto` page to know how to get Krita to recognize your plugin.


AnimLayers (Animate with Layers)
--------------------------------

    Animate specific layers. You animate a specific range of layers by prefixing the layer name with the same letters. For example *PL* then all the layers where the name starts with *PL* will be part of the animation. In the AnimLayers window you enter PL in the Key field. You can also select a layer with the wanted key and press the :guilabel:`Get key` button.

    Available here: https://github.com/thomaslynge/krita-plugins

Art Revision Control (using GIT)
--------------------------------

    Way to use Version Control systems for artwork. 

    Available here: https://github.com/abeimler/krita-plugin-durra

Bash Action (works with OSX and Linux)
--------------------------------------

    Plugin to execute Bash commands and programs as Actions on your current Images from Krita.

    Available here: https://github.com/juancarlospaco/krita-plugin-bashactions#krita-plugin-bashactions

Direct Eraser Plugin
--------------------

    Lets you create a shortcut for the eraser and brush that doesn't depend on state. In other words, pressing the eraser shortcut always sets you up with the brush in eraser mode, no toggling, no selecting the brush first. 

    Available here: https://www.mediafire.com/file/sotzc2keogz0bor/Krita+Direct+Eraser+Plugin.zip

Krita Plugin Generator
----------------------
    Generates a Plugin Template for Krita.

    Available here: https://github.com/cg-cnu/vscode-krita-plugin-generator

Mirror Fix
----------

    Allows more flexibility when mirroring, with different orientations, and selections. 

    Available here: https://github.com/EyeOdin/mirror_fix

On-screen Canvas Shortcuts
--------------------------

    An onscreen button bar with shortcuts for Krita.

    Available here: https://github.com/qeshi/henriks-onscreen-krita-shortcut-buttons/tree/master/henriks_krita_buttons

Pigment.O
---------

    Available here: https://github.com/EyeOdin/Pigment.O

Post images on Mastodon
-----------------------

    A plugin that lets you post a copy of your current document directly to Mastodon.

    Available here: https://github.com/spaceottercode/kritatoot

Python auto-complete for text editors
-------------------------------------

    If you have the Krita source code, you can use this to generate the auto-complete file for Python. Many Python editors need a :file:`.PY` file to read for auto-complete information. This script reads the C++ header files and creates a Python file that can be used for auto-completion.

    Available here: https://github.com/scottpetrovic/krita-python-auto-complete


QuickColor
----------
    
    Enables the user to swap between predefined colors by using hotkeys.

    Available here: https://github.com/JonasLW/QuickColor


Reference Image Docker (old style)
----------------------------------
    After activating the docker, click on :guilabel:`Open` and choose an image. Beware, no verification is done on the format... You've been warned! After that you can move the reference, zoom or pick a color. 

    Available here: https://github.com/antoine-roux/krita-plugin-reference

Spine File Format Export
------------------------

    Inspired by the official Photoshop Plugin, it works nearly the same. Click on :menuselection:`Tools --> Scripts --> Export to Spine`, select a folder and all your images will be exported into it as well as :file:`spine.json`.

    Available here: https://github.com/chartinger/krita-unofficial-spine-export

Tablet Controls Docker
----------------------

    Available Here: https://github.com/tokyogeometry/tabui
    
ThreeSlots
----------

    Creates three brush tool shortcuts that memorize last used brush preset for each slot independently from each other.

    Available here: https://github.com/DarkDefender/threeslots

Timer Watch
-----------

    Allows you to check the time progress, start and pause if you want to take a break, and even do alarms.

    Available here: https://github.com/EyeOdin/timer_watch
    

ToggleRefLayer 
--------------
    Enables you to assign a keyboard shortcut to toggle the visibility of a reference layer named "reference".

    Available here: https://drive.google.com/file/d/11O8FiejleajsT_uHd4Q4VBrCrYX9Rh5v/view?usp=sharing


See Something We Missed?
========================
Have a resource you made and want to share it with other artists? Let us know in the forum or visit our chat room to discuss getting the resource added to here.

.. note:: We have curated a list of community created resources for Krita. These resources will be hosted on external website, which is not under the control of Krita or KDE. Please report any error or corrections in the content to the Krita developers.
