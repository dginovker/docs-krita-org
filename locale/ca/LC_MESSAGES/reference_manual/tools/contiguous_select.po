# Translation of docs_krita_org_reference_manual___tools___contiguous_select.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:32+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Limit to Current Layer"
msgstr "Limita a la capa actual"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../<rst_epilog>:76
msgid ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: toolselectcontiguous"
msgstr ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: eina de selecció d'àrees contigües"

#: ../../reference_manual/tools/contiguous_select.rst:1
msgid "Krita's contiguous selection tool reference."
msgstr "Referència de l'eina Selecció d'àrees contigües del Krita."

#: ../../reference_manual/tools/contiguous_select.rst:11
msgid "Selection"
msgstr "Selecció"

#: ../../reference_manual/tools/contiguous_select.rst:11
msgid "Contiguous Selection"
msgstr "Selecció d'àrees contigües"

#: ../../reference_manual/tools/contiguous_select.rst:11
msgid "Magic Wand"
msgstr "Vareta màgica"

#: ../../reference_manual/tools/contiguous_select.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/contiguous_select.rst:16
msgid "Contiguous Selection Tool"
msgstr "Eina de selecció d'àrees contigües"

#: ../../reference_manual/tools/contiguous_select.rst:18
msgid "|toolselectcontiguous|"
msgstr "|toolselectcontiguous|"

#: ../../reference_manual/tools/contiguous_select.rst:20
msgid ""
"This tool, represented by a magic wand, allows you to make :ref:"
"`selections_basics` by selecting a point of color. It will select any "
"contiguous areas of a similar color to the one you selected. You can adjust "
"the \"fuzziness\" of the tool in the tool options dock. A lower number will "
"select colors closer to the color that you chose in the first place."
msgstr ""
"Aquesta eina està representada per una vareta màgica, permet fer :ref:"
"`selections_basics` seleccionant un punt de color. Haureu de seleccionar "
"qualsevol àrea contigua d'un color similar al que heu seleccionat. Podreu "
"ajustar la «borrositat» de l'eina a l'acoblador Opcions de l'eina. Un número "
"més baix seleccionarà els colors més propers al color que heu triar en "
"primer lloc."

#: ../../reference_manual/tools/contiguous_select.rst:23
msgid "Hotkeys and Sticky keys"
msgstr "Dreceres i tecles apegaloses"

#: ../../reference_manual/tools/contiguous_select.rst:25
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` estableix la selecció a «Substitueix» a les Opcions de l'eina, és "
"el mode predeterminat."

#: ../../reference_manual/tools/contiguous_select.rst:26
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` estableix la selecció a «Afegeix» a les Opcions de l'eina."

#: ../../reference_manual/tools/contiguous_select.rst:27
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ":kbd:`S` estableix la selecció a «Sostreu» a les Opcions de l'eina."

#: ../../reference_manual/tools/contiguous_select.rst:28
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Majús. + feu` |mouseleft| estableix la selecció subsegüent a "
"«Afegeix». Podeu alliberar la tecla :kbd:`Majús.` mentre s'arrossega, però "
"encara serà establerta a «Afegeix». El mateix per a les altres."

#: ../../reference_manual/tools/contiguous_select.rst:29
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt + feu` |mouseleft| estableix la selecció subsegüent a «Sostreu»."

#: ../../reference_manual/tools/contiguous_select.rst:30
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl + feu` |mouseleft| estableix la selecció subsegüent a "
"«Substitueix»."

#: ../../reference_manual/tools/contiguous_select.rst:31
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Majús. + Alt + feu` |mouseleft| estableix la selecció subsegüent a "
"«Interseca»."

#: ../../reference_manual/tools/contiguous_select.rst:35
msgid "Hovering over a selection allows you to move it."
msgstr "Passar el cursor sobre una selecció permet moure-la."

#: ../../reference_manual/tools/contiguous_select.rst:36
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Fer |mouseright| obrirà un menú ràpid de selecció amb la possibilitat "
"d'editar la selecció, entre d'altres."

#: ../../reference_manual/tools/contiguous_select.rst:40
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Podeu canviar el comportament de la tecla :kbd:`Alt` per utilitzar la tecla :"
"kbd:`Ctrl` en lloc d'alternar el canvi als :ref:`general_settings`."

#: ../../reference_manual/tools/contiguous_select.rst:43
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/contiguous_select.rst:45
msgid "Anti-aliasing"
msgstr "Antialiàsing"

#: ../../reference_manual/tools/contiguous_select.rst:46
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Alterna entre donar o no seleccions amb vores suaus. Hi ha qui prefereix "
"vores precises per a les seves seleccions."

#: ../../reference_manual/tools/contiguous_select.rst:47
msgid "Fuzziness"
msgstr "Difuminat"

#: ../../reference_manual/tools/contiguous_select.rst:48
msgid ""
"This controls whether or not the contiguous selection sees another color as "
"a border."
msgstr ""
"Controla si la selecció contigua veurà o no un altre color com una vora."

#: ../../reference_manual/tools/contiguous_select.rst:49
msgid "Grow/Shrink selection."
msgstr "Fes créixer/encongir la selecció."

#: ../../reference_manual/tools/contiguous_select.rst:50
msgid "This value extends/contracts the shape beyond its initial size."
msgstr "Aquest valor estén/contrau la forma més enllà de la seva mida inicial."

#: ../../reference_manual/tools/contiguous_select.rst:51
msgid "Feathering"
msgstr "Selecció suau"

#: ../../reference_manual/tools/contiguous_select.rst:52
msgid "This value will add a soft border to the selection-shape."
msgstr "Aquest valor afegirà una vora suau a la forma de la selecció."

#: ../../reference_manual/tools/contiguous_select.rst:54
msgid ""
"Activating this will prevent the fill tool from taking other layers into "
"account."
msgstr ""
"Activar això evitarà que l'eina d'emplenat tingui en compte les altres capes."
