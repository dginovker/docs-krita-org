# Translation of docs_krita_org_reference_manual___preferences___tablet_settings.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 16:10+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/preferences/tablet_settings.rst:1
msgid "Configuring the tablet in Krita."
msgstr "Configurar la tauleta al Krita."

#: ../../reference_manual/preferences/tablet_settings.rst:12
#: ../../reference_manual/preferences/tablet_settings.rst:21
msgid "Tablet"
msgstr "Tauleta"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Preferences"
msgstr "Preferències"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Settings"
msgstr "Ajustaments"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Pressure Curve"
msgstr "Corba de pressió"

#: ../../reference_manual/preferences/tablet_settings.rst:17
msgid "Tablet Settings"
msgstr "Ajustaments per a la tauleta"

#: ../../reference_manual/preferences/tablet_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Tablet_Settings.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Tablet_Settings.png"

#: ../../reference_manual/preferences/tablet_settings.rst:22
msgid ""
"Input Pressure Global Curve : This is the global curve setting that your "
"tablet will use in Krita. The settings here will make your tablet feel soft "
"or hard globally."
msgstr ""
"Corba global de la pressió de l'entrada: Aquest és l'ajustament de la corba "
"global que la tauleta utilitzarà en el Krita. Aquests ajustaments faran que "
"la tauleta se senti tova o dura a nivell global."

#: ../../reference_manual/preferences/tablet_settings.rst:24
msgid ""
"Some tablet devices don't tell us whether the side buttons on a stylus. If "
"you have such a device, you can try activate this workaround. Krita will try "
"to read right and middle-button clicks as if they were coming from a mouse "
"instead of a tablet. It may or may not work on your device (depends on the "
"tablet driver implementation). After changing this option Krita should be "
"restarted."
msgstr ""
"Alguns dispositius de tauleta no ens indiquen els botons laterals d'un "
"llapis. Si teniu un dispositiu d'aquest tipus, podríeu intentar activar "
"aquesta solució alternativa. El Krita intentarà llegir els clics drets i "
"migs com si vinguessin d'un ratolí en lloc d'una tauleta. Podria o no "
"funcionar en el vostre dispositiu (dependrà de la implementació del "
"controlador de la tauleta). Després de canviar aquesta opció s'haurà de "
"tornar a iniciar el Krita."

#: ../../reference_manual/preferences/tablet_settings.rst:26
msgid "Use Mouse Events for Right and Middle clicks."
msgstr "Usa els esdeveniments del ratolí per al clic del botó dret i mig."

#: ../../reference_manual/preferences/tablet_settings.rst:29
msgid "On Windows 8 or above only."
msgstr "Només al Windows 8 o superior."

#: ../../reference_manual/preferences/tablet_settings.rst:31
msgid "WinTab"
msgstr "WinTab"

#: ../../reference_manual/preferences/tablet_settings.rst:32
msgid ""
"Use the WinTab API to receive tablet pen input. This is the API being used "
"before Krita 3.3. This option is recommended for most Wacom tablets."
msgstr ""
"Utilitza l'API de WinTab per a rebre l'entrada del llapis de la tauleta. "
"Aquesta és l'API que s'utilitzava abans del Krita 3.3. Es recomana aquesta "
"opció per a la majoria de les tauletes Wacom."

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid "For Krita 3.3 or later:Tablet Input API"
msgstr "Per al Krita 3.3 o posterior: API d'entrada de la tauleta"

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid "Windows 8+ Pointer Input"
msgstr "Entrada de l'apuntador de Windows 8+"

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid ""
"Use the Pointer Input messages to receive tablet pen input. This option "
"depends on Windows Ink support from the tablet driver. This is a relatively "
"new addition so it's still considered to be experimental, but it should work "
"well enough for painting. You should try this if you are using an N-Trig "
"device (e.g. recent Microsoft Surface devices) or if your tablet does not "
"work well with WinTab."
msgstr ""
"Utilitza els missatges d'entrada del punter per a rebre l'entrada del llapis "
"de la tauleta. Aquesta opció dependrà de la compatibilitat amb Windows Ink "
"del controlador de la tauleta. Aquesta és una addició relativament nova, de "
"manera que encara es considera experimental, però hauria de funcionar prou "
"bé per a pintar. Haureu d'intentar això si utilitzeu un dispositiu N-Trig "
"(p. ex., dispositius Microsoft Surface recents) o si la vostra tauleta no "
"funciona bé amb WinTab."

#: ../../reference_manual/preferences/tablet_settings.rst:37
msgid "Advanced Tablet Settings for WinTab"
msgstr "Ajustaments avançats de la tauleta per a WinTab"

#: ../../reference_manual/preferences/tablet_settings.rst:41
msgid ".. image:: images/preferences/advanced-settings-tablet.png"
msgstr ".. image:: images/preferences/advanced-settings-tablet.png"

#: ../../reference_manual/preferences/tablet_settings.rst:42
msgid ""
"When using multiple monitors or using a tablet that is also a screen, Krita "
"will get conflicting information about how big your screen is, and sometimes "
"if it has to choose itself, there will be a tablet offset. This window "
"allows you to select the appropriate screen resolution."
msgstr ""
"En utilitzar múltiples monitors o en utilitzar una tauleta que també és una "
"pantalla, el Krita obtindrà informació contradictòria sobre com de gran és "
"la pantalla i, de vegades, si ha de triar per si mateix, hi haurà un "
"desplaçament de la tauleta. Aquesta finestra permet seleccionar la resolució "
"apropiada de la pantalla."

#: ../../reference_manual/preferences/tablet_settings.rst:44
msgid "Use Information Provided by Tablet"
msgstr "Usa la informació proporcionada per la tauleta"

#: ../../reference_manual/preferences/tablet_settings.rst:45
msgid "Use the information as given by the tablet."
msgstr "Utilitza la informació donada per la tauleta."

#: ../../reference_manual/preferences/tablet_settings.rst:46
msgid "Map to entire virtual screen"
msgstr "Mapa a una pantalla virtual sencera"

#: ../../reference_manual/preferences/tablet_settings.rst:47
msgid "Use the information as given by Windows."
msgstr "Utilitza la informació donada per Windows."

#: ../../reference_manual/preferences/tablet_settings.rst:49
msgid ""
"Type in the numbers manually. Use this when you have tried the other "
"options. You might even need to do trial and error if that is the case, but "
"at the least you can configure it."
msgstr ""
"Escriviu els números manualment. Utilitzeu-la quan hàgiu provat les altres "
"opcions. Potser fins i tot hàgiu de fer una prova i error si aquest és el "
"cas, però almenys podreu configurar-ho."

#: ../../reference_manual/preferences/tablet_settings.rst:51
msgid "Map to Custom Area"
msgstr "Mapa a una àrea personalitzada"

#: ../../reference_manual/preferences/tablet_settings.rst:51
msgid ""
"If you have a dual monitor setup and only the top half of the screen is "
"reachable, you might have to enter the total width of both screens plus the "
"double height of your monitor in this field."
msgstr ""
"Si teniu una configuració de monitor dual i només es pot accedir a la meitat "
"superior de la pantalla, és possible que hàgiu d'introduir en aquest camp "
"l'amplada total d'ambdues pantalles més el doble de l'alçada del monitor."

#: ../../reference_manual/preferences/tablet_settings.rst:55
msgid ""
"To access this dialog in Krita versions older than 4.2, you had to do the "
"following:"
msgstr ""
"Per accedir a aquest diàleg en versions del Krita anteriors a la 4.2, haureu "
"de fer el següent:"

#: ../../reference_manual/preferences/tablet_settings.rst:57
msgid "Put your stylus away from the tablet."
msgstr "Poseu el llapis lluny de la tauleta."

#: ../../reference_manual/preferences/tablet_settings.rst:58
msgid ""
"Start Krita without using a stylus, that is using a mouse or a keyboard."
msgstr ""
"Inicieu el Krita sense utilitzar un llapis òptic, això serà amb un ratolí o "
"un teclat."

#: ../../reference_manual/preferences/tablet_settings.rst:59
msgid "Press the :kbd:`Shift` key and hold it."
msgstr "Premeu la tecla :kbd:`Majús.` i manteniu-la."

#: ../../reference_manual/preferences/tablet_settings.rst:60
msgid "Touch a tablet with your stylus so Krita would recognize it."
msgstr "Toqueu la tauleta amb el llapis òptic perquè el Krita el reconegui."

#: ../../reference_manual/preferences/tablet_settings.rst:62
msgid ""
"If adjusting this doesn't work, and if you have a Wacom tablet, an offset in "
"the canvas can be caused by a faulty Wacom preference file which is not "
"removed or replaced by reinstalling the drivers."
msgstr ""
"Si l'ajust no funciona, i si teniu una tauleta Wacom, un desplaçament en el "
"llenç pot estar provocat per un fitxer de preferències de Wacom defectuós "
"que no s'elimina o substitueix reinstal·lant els controladors."

#: ../../reference_manual/preferences/tablet_settings.rst:64
msgid ""
"To fix it, use the “Wacom Tablet Preference File Utility” to clear all the "
"preferences. This should allow Krita to detect the correct settings "
"automatically."
msgstr ""
"Per a solucionar-ho, utilitzeu la «Utilitat per a fitxers de preferències de "
"la tauleta Wacom» per esborrar totes les preferències. Això hauria de "
"permetre al Krita detectar de manera automàtica els ajustaments correctes."

#: ../../reference_manual/preferences/tablet_settings.rst:67
msgid ""
"Clearing all wacom preferences will reset your tablet's configuration, thus "
"you will need to recalibrate/reconfigure it."
msgstr ""
"Esborrar totes les preferències de Wacom restablirà la configuració de la "
"tauleta, pel que haureu de tornar-la a calibrar/configurar."

#: ../../reference_manual/preferences/tablet_settings.rst:70
msgid "Tablet Tester"
msgstr "Provador de la tauleta"

#: ../../reference_manual/preferences/tablet_settings.rst:74
msgid ""
"This is a special feature for debugging tablet input. When you click on it, "
"it will open a window with two sections. The left section is the **Drawing "
"Area** and the right is the **Text Output**."
msgstr ""
"Aquesta és una característica especial per a depurar l'entrada de la "
"tauleta. En fer-hi clic, s'obrirà una finestra amb dues seccions. La secció "
"esquerra és l'**Àrea de dibuix** i la dreta és la **Sortida de text**."

#: ../../reference_manual/preferences/tablet_settings.rst:76
msgid ""
"If you draw over the Drawing Area, you will see a line appear. If your "
"tablet is working it should be both a red and blue line."
msgstr ""
"Si dibuixeu sobre l'Àrea de dibuix, veureu aparèixer una línia. Si la "
"tauleta està funcionant, haurà de ser una línia vermella i blava."

#: ../../reference_manual/preferences/tablet_settings.rst:78
msgid ""
"The red line represents mouse events. Mouse events are the most basic events "
"that Krita can pick up. However, mouse events have crude coordinates and "
"have no pressure sensitivity."
msgstr ""
"La línia vermella representa els esdeveniments del ratolí. Els esdeveniments "
"del ratolí són els esdeveniments més bàsics que el Krita pot recollir. No "
"obstant això, els esdeveniments del ratolí tenen coordenades rudimentàries i "
"no tenen sensibilitat a la pressió."

#: ../../reference_manual/preferences/tablet_settings.rst:80
msgid ""
"The blue line represents the tablet events. The tablet events only show up "
"when Krita can access your tablet. These have more precise coordinates and "
"access to sensors like pressure sensitivity."
msgstr ""
"La línia blava representa els esdeveniments de la tauleta. Els esdeveniments "
"de la tauleta només es mostren quan el Krita pot accedir a la tauleta. "
"Aquests tenen coordenades més precises i accés a sensors com la sensibilitat "
"a la pressió."

#: ../../reference_manual/preferences/tablet_settings.rst:84
msgid ""
"If you have no blue line when drawing on the lefthand drawing area, Krita "
"cannot access your tablet. Check out the :ref:`page on drawing tablets "
"<drawing_tablets>` for suggestions on what is causing this."
msgstr ""
"Si no teniu una línia blava en dibuixar a l'àrea de dibuix de mà esquerra, "
"voldrà dir que el Krita no pot accedir a la tauleta. Reviseu la :ref:`pàgina "
"sobre tauletes de dibuix <drawing_tablets>` per obtenir consells sobre el "
"que pot estar causant això."

#: ../../reference_manual/preferences/tablet_settings.rst:86
msgid ""
"When you draw a line, the output on the right will show all sorts of text "
"output. This text output can be attached to a help request or a bug report "
"to figure out what is going on."
msgstr ""
"En dibuixar una línia, la sortida de la dreta mostrarà tot tipus de sortida "
"de text. Aquesta sortida de text es pot adjuntar a una sol·licitud d'ajuda o "
"a un informe d'error per esbrinar què està passant."

#: ../../reference_manual/preferences/tablet_settings.rst:89
msgid "External Links"
msgstr "Enllaços externs"

# skip-rule: t-acc_obe
#: ../../reference_manual/preferences/tablet_settings.rst:91
msgid ""
"`David Revoy wrote an indepth guide on using this feature to maximum "
"advantage. <https://www.davidrevoy.com/article182/calibrating-wacom-stylus-"
"pressure-on-krita>`_"
msgstr ""
"`En David Revoy ha escrit una guia detallada sobre l'ús d'aquesta "
"característica per obtenir el màxim avantatge <https://www.davidrevoy.com/"
"article182/calibrating-wacom-stylus-pressure-on-krita>`_."
