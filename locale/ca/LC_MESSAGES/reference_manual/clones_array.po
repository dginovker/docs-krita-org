# Translation of docs_krita_org_reference_manual___clones_array.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-12 11:54+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Angle"
msgstr "Angle"

#: ../../reference_manual/clones_array.rst:1
msgid "The Clones Array functionality in Krita"
msgstr "La funcionalitat per a matriu de clonades al Krita"

#: ../../reference_manual/clones_array.rst:10
#: ../../reference_manual/clones_array.rst:15
msgid "Clones Array"
msgstr "Matriu de clonades"

#: ../../reference_manual/clones_array.rst:10
msgid "Clone"
msgstr "Clonar"

#: ../../reference_manual/clones_array.rst:17
msgid ""
"Allows you to create a set of clone layers quickly. These are ordered in "
"terms of rows and columns. The default options will create a 2 by 2 grid. "
"For setting up tiles of an isometric game, for example, you'd want to set "
"the X offset of the rows to half the value input into the X offset for the "
"columns, so that rows are offset by half. For a hexagonal grid, you'd want "
"to do the same, but also reduce the Y offset of the grids by the amount of "
"space the hexagon can overlap with itself when tiled."
msgstr ""
"Permet crear un conjunt de capes clonades amb rapidesa. Aquestes estaran "
"ordenades en termes de files i columnes. Les opcions predeterminades crearan "
"una quadrícula de 2 per 2. Per exemple, per a configurar els mosaics d'un "
"joc isomètric, voldreu establir el desplaçament X de les files a la meitat "
"del valor introduït en el desplaçament X per a les columnes, de manera que "
"les files es desplacin la meitat. Per a una quadrícula hexagonal, voldríeu "
"fer el mateix, però també reduir el desplaçament Y de les quadrícules per la "
"quantitat d'espai que l'hexàgon pot solapar quan està en mosaic."

#: ../../reference_manual/clones_array.rst:19
msgid "\\- Elements"
msgstr "\\- elements"

#: ../../reference_manual/clones_array.rst:20
msgid ""
"The amount of elements that should be generated using a negative of the "
"offset."
msgstr ""
"La quantitat d'elements que s'hauran de generar emprant un desplaçament "
"negatiu."

#: ../../reference_manual/clones_array.rst:21
msgid "\\+ Elements"
msgstr "\\+ elements"

#: ../../reference_manual/clones_array.rst:22
msgid ""
"The amount of elements that should be generated using a positive of the "
"offset."
msgstr ""
"La quantitat d'elements que s'hauran de generar emprant un desplaçament "
"positiu."

#: ../../reference_manual/clones_array.rst:23
msgid "X offset"
msgstr "Desplaçament X"

#: ../../reference_manual/clones_array.rst:24
msgid ""
"The X offset in pixels. Use this in combination with Y offset to position a "
"clone using Cartesian coordinates."
msgstr ""
"El desplaçament X en píxels. Utilitzeu-la en combinació amb el desplaçament "
"Y per a posicionar una clonada utilitzant les coordenades cartesianes."

#: ../../reference_manual/clones_array.rst:25
msgid "Y offset"
msgstr "Desplaçament Y"

#: ../../reference_manual/clones_array.rst:26
msgid ""
"The Y offset in pixels. Use this in combination with X offset to position a "
"clone using Cartesian coordinates."
msgstr ""
"El desplaçament Y en píxels. Utilitzeu-la en combinació amb el desplaçament "
"X per a posicionar una clonada utilitzant les coordenades cartesianes."

#: ../../reference_manual/clones_array.rst:27
msgid "Distance"
msgstr "Distància"

#: ../../reference_manual/clones_array.rst:28
msgid ""
"The line-distance of the original origin to the clones origin. Use this in "
"combination with angle to position a clone using a polar coordinate system."
msgstr ""
"La distància de la línia de l'origen original fins a l'origen de les "
"clonades. Utilitzeu-la en combinació amb l'angle per a posicionar una "
"clonada emprant un sistema de coordenades polars."

#: ../../reference_manual/clones_array.rst:30
msgid ""
"The angle-offset of the column or row. Use this in combination with distance "
"to position a clone using a polar coordinate system."
msgstr ""
"L'angle de desplaçament de la columna o fila. Utilitzeu-la en combinació amb "
"la distància per a posicionar una clonada emprant un sistema de coordenades "
"polars."
