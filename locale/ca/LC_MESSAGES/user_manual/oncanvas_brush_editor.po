# Translation of docs_krita_org_user_manual___oncanvas_brush_editor.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 16:08+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../user_manual/oncanvas_brush_editor.rst:1
msgid "Using the oncanvas brush editor in Krita."
msgstr "Utilitzant l'editor de pinzells en el llenç en el Krita."

#: ../../user_manual/oncanvas_brush_editor.rst:11
msgid "Brush Settings"
msgstr "Ajustaments del pinzell"

#: ../../user_manual/oncanvas_brush_editor.rst:11
msgid "Pop-up Palette"
msgstr "Paleta emergent"

#: ../../user_manual/oncanvas_brush_editor.rst:16
msgid "On-Canvas Brush Editor"
msgstr "Editor de pinzells en el llenç"

#: ../../user_manual/oncanvas_brush_editor.rst:19
msgid ""
"Krita's brush editor is, as you may know, on the :kbd:`F5` key. However, "
"sometimes you just want to modify a single parameter quickly. Perhaps even "
"in canvas-only mode. The on canvas brush editor or brush HUD allows you to "
"do this. It's accessible from the pop-up palette, by ticking the lower-right "
"arrow button."
msgstr ""
"L'editor de pinzells del Krita està, com sabreu, a la tecla :kbd:`F5`. No "
"obstant això, de vegades només voldreu modificar un únic paràmetre amb "
"rapidesa. Potser fins i tot només en el mode llenç. L'editor de pinzells en "
"el llenç o HUD, permet fer-ho possible. S'hi pot accedir des de la paleta "
"emergent, fent clic al botó de fletxa dreta que hi ha a la part inferior."

#: ../../user_manual/oncanvas_brush_editor.rst:26
msgid ".. image:: images/On_canvas_brush_editor.png"
msgstr ".. image:: images/On_canvas_brush_editor.png"

#: ../../user_manual/oncanvas_brush_editor.rst:27
msgid ""
"You can change the amount of visible settings and their order by clicking "
"the settings icon next to the brush name."
msgstr ""
"Podeu canviar la quantitat d'ajustaments visibles i el seu ordre fent clic a "
"la icona d'ajustaments que hi ha al costat del nom del pinzell."

#: ../../user_manual/oncanvas_brush_editor.rst:31
msgid ".. image:: images/On_canvas_brush_editor_2.png"
msgstr ".. image:: images/On_canvas_brush_editor_2.png"

#: ../../user_manual/oncanvas_brush_editor.rst:32
msgid ""
"On the left are all unused settings, on the right are all used settings. You "
"use the :guilabel:`>` and :guilabel:`<` buttons to move a setting between "
"the two columns. The :guilabel:`Up` and :guilabel:`Down` buttons allow you "
"to adjust the order of the used settings, for when you think flow is more "
"important than size."
msgstr ""
"A l'esquerra hi ha tots els ajustaments no utilitzats, al costat dret hi ha "
"tots els ajustaments utilitzats. Utilitzeu els botons :guilabel:`>` i :"
"guilabel:`<` per a moure un ajustament entre les dues columnes. Els botons :"
"guilabel:`Amunt` i :guilabel:`Avall` permeten ajustar l'ordre dels "
"ajustaments emprats, ja que com penseu, el Flux és més important que la Mida."

#: ../../user_manual/oncanvas_brush_editor.rst:38
msgid ".. image:: images/On_canvas_brush_editor_3.png"
msgstr ".. image:: images/On_canvas_brush_editor_3.png"

#: ../../user_manual/oncanvas_brush_editor.rst:39
msgid ""
"These set-ups are PER brush engine, so different brush engines can have "
"different configurations."
msgstr ""
"Aquests ajustaments són per al motor de pinzell, de manera que els diferents "
"motors de pinzell podran tenir configuracions diferents."
