# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:23+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Activate Angle Snap"
msgstr "Aktivera vinkellåsning"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../<rst_epilog>:72
msgid ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: toolselectpath"
msgstr ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: Konturmarkeringsverktyg"

#: ../../reference_manual/tools/path_select.rst:1
msgid "Krita's bezier curve selection tool reference."
msgstr "Referens för Kritas Bezier konturmarkeringsverktyg."

#: ../../reference_manual/tools/path_select.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Vector"
msgstr "Vektor"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Path"
msgstr "Kontur"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Bezier Curve"
msgstr "Bezierkurva"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Pen"
msgstr "Penna"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Selection"
msgstr "Markering"

#: ../../reference_manual/tools/path_select.rst:17
msgid "Path Selection Tool"
msgstr "Konturmarkeringsverktyg"

#: ../../reference_manual/tools/path_select.rst:19
msgid "|toolselectpath|"
msgstr "|toolselectpath|"

#: ../../reference_manual/tools/path_select.rst:21
msgid ""
"This tool, represented by an ellipse with a dashed border and a curve "
"control, allows you to make a :ref:`selections_basics` of an area by drawing "
"a path around it. Click where you want each point of the path to be. Click "
"and drag to curve the line between points. Finally click on the first point "
"you created to close your path."
msgstr ""
"Verktyget representeras av en ellips med en streckad kant och en "
"kurvstyrning som låter dig skapa en :ref:`selections_basics` för ett område "
"genom att rita en kontur omkring det. Klicka där varje punkt i konturen ska "
"vara. Klicka och dra för att göra linjen till kurva mellan punkterna. Klicka "
"till sist på den första punkten som skapades för att sluta konturen."

#: ../../reference_manual/tools/path_select.rst:24
msgid "Hotkeys and Sticky keys"
msgstr "Snabbtangenter och klistriga tangenter"

#: ../../reference_manual/tools/path_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` ställer in markeringen till 'ersätt' i verktygsalternativen, vilket "
"är standardinställningen."

#: ../../reference_manual/tools/path_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` ställer in markeringen till 'addera' i verktygsalternativen."

#: ../../reference_manual/tools/path_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`S` ställer in markeringen till 'subtrahera' i verktygsalternativen."

#: ../../reference_manual/tools/path_select.rst:29
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Skift +` vänster musknapp in markeringen till 'addera'. Man kan släppa "
"upp tangenten :kbd:`Skift` medan man drar, och den ställs ändå in till 'lägg "
"till'. Samma sak för de andra."

#: ../../reference_manual/tools/path_select.rst:30
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt +` vänster musknapp ställer in efterföljande markering till "
"'subtrahera'."

#: ../../reference_manual/tools/path_select.rst:31
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl +` vänster musknapp ställer in efterföljande markering till "
"'ersätt'."

#: ../../reference_manual/tools/path_select.rst:32
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt +` vänster musknapp ställer in efterföljande markering "
"till 'snitt'."

#: ../../reference_manual/tools/path_select.rst:37
msgid "Hovering over a selection allows you to move it."
msgstr "Att hålla musen över en markering göt det möjlig att flytta den."

#: ../../reference_manual/tools/path_select.rst:38
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Att klicka med höger musknapp visar en snabbvalsmeny med bland annat "
"möjlighet att redigera markeringen."

#: ../../reference_manual/tools/path_select.rst:43
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Man kan ändra beteende hos tangenten :kbd:`Alt` så att tangenten :kbd:`Ctrl` "
"används istället genom att ställa om inställningen under :ref:"
"`general_settings`."

#: ../../reference_manual/tools/path_select.rst:46
msgid "Tool Options"
msgstr "Verktygsalternativ"

#: ../../reference_manual/tools/path_select.rst:50
#: ../../reference_manual/tools/path_select.rst:58
msgid "Autosmooth Curve"
msgstr "Jämna ut kurva automatiskt"

#: ../../reference_manual/tools/path_select.rst:51
#: ../../reference_manual/tools/path_select.rst:59
msgid ""
"Toggling this will have nodes initialize with smooth curves instead of "
"angles. Untoggle this if you want to create sharp angles for a node. This "
"will not affect curve sharpness from dragging after clicking."
msgstr ""
"Aktivera det för att noder ska initieras med jämna kurvor istället för "
"vinklar. Avmarkera det om du vill skapa skarpa vinklar för en nod. Det "
"påverkar inte kurvans skarphet från dragning efter att ha klickat."

#: ../../reference_manual/tools/path_select.rst:54
msgid "Anti-aliasing"
msgstr "Kantutjämning"

#: ../../reference_manual/tools/path_select.rst:54
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Ändrar om markeringar ska få vävkanter. Vissa människor föredrar skarpa "
"naggade kanter för sina markeringar."

#: ../../reference_manual/tools/path_select.rst:61
msgid "Angle Snapping Delta"
msgstr "Vinkellåsningsdelta"

#: ../../reference_manual/tools/path_select.rst:62
msgid "The angle to snap to."
msgstr "Vinkeln att låsa till."

#: ../../reference_manual/tools/path_select.rst:64
msgid ""
"Angle snap will make it easier to have the next line be at a specific angle "
"of the current. The angle is determined by the :guilabel:`Angle Snapping "
"Delta`."
msgstr ""
"Vinkellåsning gör det enklare att få nästa linje att ha en specifik vinkel "
"mot den nuvarande. Vinkeln bestäms av :guilabel:`Vinkellåsningsdelta`."
