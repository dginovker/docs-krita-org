# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:15+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/tools/color_selector.rst:1
msgid "Krita's color selector tool reference."
msgstr "Referens för Kritas färgväljarverktyg."

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Colors"
msgstr "Färger"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Eyedropper"
msgstr "Pipett"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Color Selector"
msgstr "Färgväljare"

#: ../../reference_manual/tools/color_selector.rst:17
msgid "Color Selector Tool"
msgstr "Färgväljare"

#: ../../reference_manual/tools/color_selector.rst:20
msgid ""
"This tool allows you to choose a point from the canvas and make the color of "
"that point the active foreground color. When a painting or drawing tool is "
"selected the Color Picker tool can also be quickly accessed by pressing the :"
"kbd:`Ctrl` key."
msgstr ""
"Verktyget gör det möjligt att välja en punkt på duken och göra punktens färg "
"den aktiva förgrundsfärgen. När ett målar- eller ritverktyg väljes, kan "
"färgväljarverktyget snabbt kommas åt genom att trycka på tangenten :kbd:"
"`Ctrl`."

#: ../../reference_manual/tools/color_selector.rst:23
msgid ".. image:: images/tools/Color_Dropper_Tool_Options.png"
msgstr ".. image:: images/tools/Color_Dropper_Tool_Options.png"

#: ../../reference_manual/tools/color_selector.rst:24
msgid ""
"There are several options shown in the :guilabel:`Tool Options` docker when "
"the :guilabel:`Color Picker` tool is active:"
msgstr ""
"Det finns flera alternativ som visas i panelen :guilabel:"
"`Verktygsalternativ` när verktyget :guilabel:`Färgväljare` är aktivt:"

#: ../../reference_manual/tools/color_selector.rst:26
msgid ""
"The first drop-down box allows you to select whether you want to sample from "
"all visible layers or only the active layer. You can choose to have your "
"selection update the current foreground color, to be added into a color "
"palette, or to do both."
msgstr ""
"Den första kombinationsrutan gör att man kan välja om samplingen ska göras "
"från alla synliga lager eller bara det aktiva lagret. Det går att välja att "
"markeringen uppdaterar aktuell förgrundsfärg, läggs till i en färgpalett, "
"eller gör bådadera."

#: ../../reference_manual/tools/color_selector.rst:30
msgid ""
"The middle section contains a few properties that change how the Color "
"Picker picks up color; you can set a :guilabel:`Radius`, which will average "
"the colors in the area around the cursor, and you can now also set a :"
"guilabel:`Blend` percentage, which controls how much color is \"soaked up\" "
"and mixed in with your current color. Read :ref:`mixing_colors` for "
"information about how the Color Picker's blend option can be used as a tool "
"for off-canvas color mixing."
msgstr ""
"Sektionen i mitten innehåller några egenskaper som ändrar hur färgväljaren "
"hämtar färg. Man kan ange en :guilabel:`Radie`, som medelvärdesbildar "
"färgerna i området omkring markören, och man kan nu också ange procentvärdet "
"guilabel:`Blandning`, som bestämmer hur mycket färg som \"sugs upp\" och "
"blandas med aktuell färg. Läs :ref:`mixing_colors` för information om hur "
"färgväljarens blandningsalternativ kan användas som verktyg för "
"färgblandning utanför duken."

#: ../../reference_manual/tools/color_selector.rst:32
msgid ""
"At the very bottom is the Info Box, which displays per-channel data about "
"your most recently picked color. Color data can be shown as 8-bit numbers or "
"percentages."
msgstr ""
"Allra längst ner finns informationsrutan, som visar information per kanal om "
"den senast hämtade färgen. Färginformation kan visas som 8-bitars tal eller "
"procentvärden."
