# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-07-07 15:20+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:1
msgid "Overview of the gamut mask docker."
msgstr "Översikt av färgomfångsmaskpanelen."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Color"
msgstr "Färg"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Color Selector"
msgstr "Färgväljare"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Gamut Mask Docker"
msgstr "Panel för färgomfångsmask"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Gamut Mask"
msgstr "Färgomfångsmask"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:16
msgid "Gamut Masks Docker"
msgstr "Panel för färgomfångsmasker"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:19
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:22
msgid "Docker for gamut masks selection and management."
msgstr "Panel för val och hantering av färgomfångsmasker."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:25
msgid "Usage"
msgstr "Användning"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:27
msgid "|mouseleft| an icon (1) to apply a mask to color selectors."
msgstr "Vänsterklicka på en ikon (1) för att använda en mask i färgväljare."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:29
msgid "Gamut Masks can be imported and exported in the resource manager."
msgstr "Färgomfångsmasker kan importeras och exporteras i resurshanteraren."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:33
msgid "Management Toolbar"
msgstr "Hanteringsverktygsrad"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:35
msgid "Create new mask (2)"
msgstr "Skapa ny mask (2)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:36
msgid "Opens the mask editor with an empty template."
msgstr "Öppnar maskeditorn med en tom mall."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:37
msgid "Edit mask (3)"
msgstr "Redigera mask (3)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:38
msgid "Opens the the currently selected mask in the editor."
msgstr "Öppna masken som för närvarande är vald i editorn."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:39
msgid "Duplicate mask (4)"
msgstr "Duplicera mask (4)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:40
msgid ""
"Creates a copy of the currently selected mask and opens the copy in the "
"editor."
msgstr ""
"Skapar en kopia av masken som för närvarande är vald och öppnar kopian i "
"editorn."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:42
msgid "Deletes the currently selected mask."
msgstr "Tar bort masken som för närvarande är vald."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:43
msgid "Delete mask (5)"
msgstr "Ta bort mask (5)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:46
msgid "Editing"
msgstr "Redigering"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:48
msgid ""
"If you choose to create a new mask, edit, or duplicate selected mask, the "
"mask template document will be opened as a new view (1)."
msgstr ""
"Om man har valt att skapa en ny mask, redigera eller duplicera markerad "
"mask, öppnas maskens dokumentmall som en ny vy (1)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:50
msgid ""
"There you can create new shapes and modify the mask with standard vector "
"tools (:ref:`vector_graphics`)."
msgstr ""
"Där kan man skapa nya former och ändra masken med standardvektorverktyg (:"
"ref:`vector_graphics`)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:52
msgid "Fill in the fields at (2)."
msgstr "Fyll i fälten vid (2)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:54
msgid "Title (Mandatory)"
msgstr "Titel (obligatorisk)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:55
msgid "The name of the gamut mask."
msgstr "Färgomfångsmaskens namn."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:57
msgid "Description"
msgstr "Beskrivning"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:57
msgid "A description."
msgstr "En beskrivning."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:59
msgid ""
":guilabel:`Preview` the mask in the artistic color selector (4), :guilabel:"
"`save` the mask (5), or :guilabel:`cancel` editing (3)."
msgstr ""
":guilabel:`Förhandsgranska` masken i den artistiska färgväljaren (4), :"
"guilabel:`Spara` masken (5), eller :guilabel:`Avbryt` redigeringen (3)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:63
msgid ""
"The shapes need to be added to the layer named “maskShapesLayer” (which is "
"selected by default)."
msgstr ""
"Formerna måste läggas till på lagret som kallas “maskShapesLayer” (som "
"normalt är valt)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:64
msgid "The shapes need have solid background to show correctly in the editor."
msgstr ""
"Formerna måste ha en fast färgbakgrund för att visas riktigt i editorn."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:65
msgid "A template with no shapes cannot be saved."
msgstr "En mall utan former kan inte sparas."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:69
msgid ""
"The mask is intended to be composed of basic vector shapes. Although "
"interesting results might arise from using advanced vector drawing "
"techniques, not all features are guaranteed to work properly (e.g. grouping, "
"vector text, etc.)."
msgstr ""
"Masken är avsedd att sammanfogas av grundläggande vektorformer. Även om "
"intressanta resultat kan uppstå genom att använda avancerade tekniker för "
"att rita vektorer, garanteras inte att alla funktioner fungerar som de ska "
"(t.ex. gruppering, vektortext, etc.)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:72
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:74
msgid "External Info"
msgstr "Extern information"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:76
msgid ""
"`Color Wheel Masking, Part 1 by James Gurney. <https://gurneyjourney."
"blogspot.com/2008/01/color-wheel-masking-part-1.html>`_"
msgstr ""
"`Color Wheel Masking, Part 1 by James Gurney. <https://gurneyjourney."
"blogspot.com/2008/01/color-wheel-masking-part-1.html>`_"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:77
msgid ""
"`The Shapes of Color Schemes by James Gurney. <https://gurneyjourney."
"blogspot.com/2008/02/shapes-of-color-schemes.html>`_"
msgstr ""
"`The Shapes of Color Schemes by James Gurney. <https://gurneyjourney."
"blogspot.com/2008/02/shapes-of-color-schemes.html>`_"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:78
msgid ""
"`Gamut Masking Demonstration by James Gourney (YouTube). <https://youtu.be/"
"qfE4E5goEIc>`_"
msgstr ""
"`Demonstration av färgomfångsmaskering av James Gourney (YouTube). <https://"
"youtu.be/qfE4E5goEIc>`_"
