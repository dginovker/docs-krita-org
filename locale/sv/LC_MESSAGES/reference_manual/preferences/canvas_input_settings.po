# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-29 21:29+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Profile"
msgstr "Profil"

#: ../../reference_manual/preferences/canvas_input_settings.rst:1
msgid "Canvas input settings in Krita."
msgstr "Inställningar för dukinmatning i Krita."

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
#: ../../reference_manual/preferences/canvas_input_settings.rst:16
msgid "Canvas Input Settings"
msgstr "Inställningar för dukinmatning"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Preferences"
msgstr "Anpassning"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Settings"
msgstr "Inställningar"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Tablet"
msgstr "Ritplatta"

#: ../../reference_manual/preferences/canvas_input_settings.rst:18
msgid ""
"Krita has ways to set mouse and keyboard combinations for different actions. "
"The user can set which combinations to use for a certain Krita command over "
"here. This section is under development and will include more options in "
"future."
msgstr ""
"Krita har sätt att ställa in mus- och tangentbordskombinationer för olika "
"åtgärder. Användaren kan ställa in vilka kombinationer som ska användas för "
"ett visst kommando i Krita här. Avsnittet är under utveckling och kommer att "
"inkludera fler alternativ i framtiden."

#: ../../reference_manual/preferences/canvas_input_settings.rst:21
msgid "The user can make different profiles of combinations and save them."
msgstr "Användaren kan skapa olika profiler av kombinationer och spara dem."
