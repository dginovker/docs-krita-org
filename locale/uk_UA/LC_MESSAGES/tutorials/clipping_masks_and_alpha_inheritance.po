# Translation of docs_krita_org_tutorials___clipping_masks_and_alpha_inheritance.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_tutorials___clipping_masks_and_alpha_inheritance\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:45+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Composition_animation.gif\n"
"   :alt: Animation showing that groups are composed before the rest of "
"composition takes place."
msgstr ""
".. image:: images/clipping-masks/Composition_animation.gif\n"
"   :alt: Анімація, у якій показано, що групи скомпоновано до того, як "
"створюється решта композиції."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/layers/Layer-composite.png\n"
"   :alt: An image showing the way layers composite in Krita"
msgstr ""
".. image:: images/layers/Layer-composite.png\n"
"   :alt: Зображення, на якому показано компонування шарів у Krita"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/layers/Krita-tutorial2-I.1-2.png\n"
"   :alt: An image showing how the alpha inheritance works and affects layers."
msgstr ""
".. image:: images/layers/Krita-tutorial2-I.1-2.png\n"
"   :alt: Зображення, яке показує, як працює успадковування прозорості, і як "
"воно впливає на шари."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_1.png\n"
"   :alt: an image with line art and a layer for each flat of color"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_1.png\n"
"   :alt: Зображення із графікою та шар для кожної однотонної області"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_2.png\n"
"   :alt: An image showing how the alpha inheritance works and affects layers."
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_2.png\n"
"   :alt: Зображення, яке показує, як працює успадковування прозорості, і як "
"воно впливає на шари."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_3.png\n"
"   :alt: clipping mask step 3"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_3.png\n"
"   :alt: Маска обрізання, крок 3"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_4.png\n"
"   :alt: clipping mask step 4"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_4.png\n"
"   :alt: Маска обрізання, крок 4"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_5.png\n"
"   :alt: clipping mask step 5"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_5.png\n"
"   :alt: Маска обрізання, крок 5"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_6.png\n"
"   :alt: clipping mask step 6"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_6.png\n"
"   :alt: Маска обрізання, крок 6"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_7.png\n"
"   :alt: clipping mask step 7"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_7.png\n"
"   :alt: Маска обрізання, крок 7"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_clip_blur.gif\n"
"   :alt: filter layers and alpha inheritance"
msgstr ""
".. image:: images/clipping-masks/Tut_clip_blur.gif\n"
"   :alt: Шари фільтрування та успадкування прозорості"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:1
msgid "An introduction for using clipping masks in Krita."
msgstr "Вступ до використання масок прозорості у Krita."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:10
msgid "Alpha Inheritance"
msgstr "Успадкування прозорості"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:10
msgid "Clipping Masks"
msgstr "Маски обрізання"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:15
msgid "Clipping Masks and Alpha Inheritance"
msgstr "Маски обрізання та успадкування прозорості"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:17
msgid ""
"Krita doesn't have clipping mask functionality in the manner that Photoshop "
"and programs that mimic Photoshop's functionality have. That's because in "
"Krita, unlike such software, a group layer is not an arbitrary collection of "
"layers. Rather, in Krita, group layers are composited separately from the "
"rest of the stack, and then the result is added into the stack. In other "
"words, in Krita group layers are in effect a separate image inside your "
"image."
msgstr ""
"У Krita не передбачено функціональної можливості маски обрізання у такий "
"спосіб, як її передбачено у Photoshop та інших програмах, які імітують "
"роботу Photoshop. Причина полягає у тому, що у Krita, на відміну від іншого "
"програмного забезпечення, група шарів не є довільною збіркою шарів. У Krita "
"група шарів має власну композицію, не таку, як у решті стосу шарів. "
"Остаточне зображення є результатом компонування зображення у групі шарів і "
"зображення решти стосу. Іншими словами, у Krita групи шарів формують власні "
"зображення всередині загального зображення."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:24
msgid ""
"The exception is when using pass-through mode, meaning that alpha "
"inheritance won't work right when turning on pass-through on the layer."
msgstr ""
"Винятком є режим передавання. Тобто успадкування прозорості не працює, якщо "
"для шару увімкнено режим передавання прозорості."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:30
msgid ""
"When we turn on alpha inheritance, the alpha-inherited layer keeps the same "
"transparency as the layers below."
msgstr ""
"Якщо увімкнено успадкування прозорості, шар із успадкуванням прозорості "
"зберігатиме ту саму прозорість, що і шари під ним."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:36
msgid ""
"Combined with group layers this can be quite powerful. A situation where "
"this is particularly useful is the following:"
msgstr ""
"У поєднанні із групами шарів ця можливість може бути доволі потужною. "
"Ситуація, у якій це доволі корисно, є такою:"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:42
msgid ""
"Here we have an image with line art and a layer for each flat of colors. We "
"want to add complicated multi-layered shading to this, while keeping the "
"neatness of the existing color flats. To get a clipping mask working, you "
"first need to put layers into a group. You can do this by making a group "
"layer and drag-and-dropping the layers into it, or by selecting the layers "
"you want grouped and pressing the :kbd:`Ctrl + G` shortcut. Here we do that "
"with the iris and the eye-white layers."
msgstr ""
"Маємо зображення із графікою і шар для кожного простого кольору. Ми хочемо "
"додати складне багатошарове затінення, зберігши чіткість наявних кольорових "
"областей. Щоб створити ефект маски обрізання, вам спочатку слід зібрати шари "
"до групи. Для цього можна створити групу шарів і перетягнути і скинути шари "
"до неї або позначити шари, які ви хочете згрупувати і натисніть комбінацію "
"клавіш :kbd:`Ctrl + G`. У нашому прикладі ми зробили це із райдужкою та "
"шарами білків очей."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:50
msgid ""
"We add a layer for the highlight above the other two layers, and add some "
"white scribbles."
msgstr ""
"Ми додамо шар для виблисків над іншими двома шарами і додамо декілька білих "
"карлючок."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:60
msgid ""
"In the above, we have our layer with a white scribble on the left, and on "
"the right, the same layer, but with alpha inheritance active, limiting it to "
"the combined area of the iris and eye-white layers."
msgstr ""
"Вище, наш шар із білою карлючкою ліворуч, а праворуч — той самий шар, але із "
"активованим успадкуванням прозорості, який обмежує карлючку комбінованою "
"ділянкою райдужки і очного білка."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:66
msgid ""
"Now there’s an easier way to set up alpha inheritance. If you select a layer "
"or set of layers and press the :kbd:`Ctrl + Shift + G` shortcut, you create "
"a quick clipping group. That is, you group the layers, and a ‘mask layer’ "
"set with alpha inheritance is added on top."
msgstr ""
"Тепер трохи про простіший спосіб налаштовування успадковування прозорості. "
"Якщо ви позначите шар або набір шарів і натисніть комбінацію клавіш :kbd:"
"`Ctrl + Shift + G`, буде швидко створено групу обрізання. Ви групуєте шари, "
"а набір «шар маски» із успадкуванням прозорості додається згори."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:76
msgid ""
"The fact that alpha inheritance can use the composited transparency from a "
"combination of layers means that you can have a layer with the erase-"
"blending mode in between, and have that affect the area that the layer above "
"is clipped to. Above, the lower image is exactly the same as the upper one, "
"except with the erase-layer hidden. Filters can also affect the alpha "
"inheritance:"
msgstr ""
"Те, що успадкування прозорості може використовувати композитну прозорість із "
"комбінації шарів, означає, що ви можете мати шар із режимом змішування-"
"витирання між ними так, щоб він працював із ділянкою, до якої обрізано шар "
"над ним. Вище, нижнє зображення є таким самим, що і верхнє зображення, але з "
"приховуванням шару витирання (eraselayer). Фільтри також можуть впливати на "
"успадкування прозорості:"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:83
msgid ""
"Above, the blur filter layer gives different results when in different "
"places, due to different parts being blurred."
msgstr ""
"Вище, застосування шару фільтра розмивання дає різні результати у різних "
"місцях, оскільки розмиванню підлягають різні частини зображення."
