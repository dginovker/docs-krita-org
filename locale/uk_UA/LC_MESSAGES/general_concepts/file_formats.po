# Translation of docs_krita_org_general_concepts___file_formats.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_general_concepts___file_formats\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:43+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats.rst:1
msgid "The file formats category."
msgstr "Категорія форматів файлів."

#: ../../general_concepts/file_formats.rst:14
msgid "File Formats"
msgstr "Формати файлів"

#: ../../general_concepts/file_formats.rst:16
msgid ""
"This category is for graphics file-formats. While most file-formats can be "
"looked up on wikipedia, this doesn't always explain what the format can be "
"used for and what its strengths and weaknesses are."
msgstr ""
"У цій категорії зібрано дані щодо форматів файлів із графічними даними. Хоча "
"дані щодо більшості форматів файлів можна знайти у Вікіпедії, на відповідних "
"сторінках не завжди є пояснення того, для яких даних можна використовувати "
"той чи інший формат, та того, якими є переваги та недоліки формату."

#: ../../general_concepts/file_formats.rst:18
msgid ""
"In this category we try to describe these in a manner that can be read by "
"beginners."
msgstr ""
"У цій категорії ми намагаємося описати ці формати файлів у спосіб, який буде "
"зрозумілим для початківців."

#: ../../general_concepts/file_formats.rst:20
msgid ""
"Generally, there are the following features that people pay attention to in "
"regards to file formats:"
msgstr ""
"Загалом, при обговоренні форматів файлів звертають увагу на такі можливості:"

#: ../../general_concepts/file_formats.rst:23
msgid "Compression"
msgstr "Стискання"

#: ../../general_concepts/file_formats.rst:25
msgid ""
"Compression is how the file-format tries to describe the image with as "
"little data as possible, so that the resulting file is as small as it can "
"get without losing quality."
msgstr ""
"Стискання визначає спосіб, у який стандартом формату передбачено "
"використання для зображення якомога меншого місця на пристрої для зберігання "
"даних, тобто алгоритм, за яким дані у файлі певного формату роблять якомога "
"меншими за обсягом без втрати якості зображення."

#: ../../general_concepts/file_formats.rst:27
msgid ""
"What we generally see is that formats that are small on disk either lose "
"image quality, or require the computer to spend a lot of time thinking about "
"how the image should look."
msgstr ""
"Загалом, можна зауважити, що використання форматів, які дають найменші за "
"розміром файли на диску, передбачає або втрату якості зображення, або "
"потребує значних обчислювальних потужностей для відновлення початкових даних "
"зображення, які запаковано до файла."

#: ../../general_concepts/file_formats.rst:29
msgid ""
"Vector file-formats like ``SVG`` are a typical example of the latter. They "
"are really small because the technology used to create them is based on "
"mathematics, so it only stores maths-variables and can achieve very high "
"quality. The downside is that the computer needs to spend a lot of time "
"thinking about how it should look, and sometimes different programs have "
"different ways of interpreting the values. Furthermore, vector file-formats "
"imply vector graphics, which is a very different way of working than Krita "
"is specialized in."
msgstr ""
"Векторні формати файлів, зокрема ``SVG``, є типовим прикладом другого "
"різновиду форматів. Файли у цих форматах є дуже малими за розміром, оскільки "
"технологію, яку використано для їхнього створення засновано на математиці, "
"тому у файлах зберігаються лише значення математичних змінних, а якість "
"зображення близька до ідеальної. Недоліком є те, що комп'ютеру потрібно "
"доволі багато часу для визначення того, як має виглядати зображення. Крім "
"того, іноді різні програми у різний спосіб інтерпретують записані у файлі "
"значення. Крім того, векторні формати зберігають дані векторної графіки, яка "
"передбачає зовсім інший підхід до створення малюнків, ніж той, на якому "
"спеціалізується Krita."

#: ../../general_concepts/file_formats.rst:31
msgid ""
":ref:`Lossy file formats <lossy_compression>`, like ``JPG`` or ``WebP`` are "
"an example of small on disk, but lowering the quality, and are best used for "
"very particular types of images. Lossy thus means that the file format plays "
"fast and loose with describing your image to reduce filesize."
msgstr ""
":ref:`Формати файлів із втратою якості <lossy_compression>`, зокрема ``JPG`` "
"або ``WebP``, є прикладом форматів, які дають малі файли на диску, але "
"знижують якість зображення, а також пасують лише для зображень певного типу. "
"Втрата якості означає, що стандартом формату передбачено швидкий і неточний "
"опис зображення з метою зменшення розміру файла, у якому зберігаються дані "
"зображення."

#: ../../general_concepts/file_formats.rst:33
msgid ""
":ref:`Non-lossy or lossless formats <lossless_compression>`, like ``PNG``, "
"``GIF`` or ``BMP`` are in contrast, much heavier on disk, but much more "
"likely to retain quality."
msgstr ""
":ref:`Формати без втрати <lossless_compression>`, зокрема ``PNG``, ``GIF`` "
"або ``BMP``, навпаки, дають більші файли на диску, але із більшою "
"ймовірністю зберігають якість зображення."

#: ../../general_concepts/file_formats.rst:35
msgid ""
"Then, there's proper working file formats like Krita's ``KRA``, Gimp's "
"``XCF``, Photoshop's ``PSD``, but also interchange formats like ``ORA`` and "
"``EXR``. These are the heaviest on the hard-drive and often require special "
"programs to open them up, but on the other hand these are meant to keep your "
"working environment intact, and keep all the layers and guides in them."
msgstr ""
"Крім того, є суто робочі формати, зокрема формат Krita ``KRA``, формат GIMP "
"``XCF``, формат Photoshop ``PSD``, а також формати для обміну даними, "
"зокрема ``ORA`` та ``EXR``. Файли у цих форматах є найоб'ємнішими і часто "
"потребують для відкриття встановлення спеціалізованих програм, але, з іншого "
"боку, вони містять усі дані робочого середовища і зберігають усі шари та "
"напрямні зображення."

#: ../../general_concepts/file_formats.rst:38
msgid "Metadata"
msgstr "Метадані"

#: ../../general_concepts/file_formats.rst:40
msgid ""
"Metadata is the ability of a file format to contain information outside of "
"the actual image contents. This can be human readable data, like the date of "
"creation, the name of the author, a description of the image, but also "
"computer readable data, like an icc-profile which tells the computer about "
"the qualities of how the colors inside the file should be read."
msgstr ""
"Метадані формату файлів — можливість формату зберігати дані, окрім самих "
"даних зображення. Метаданими можуть бути дані, які призначено для читання "
"людиною, зокрема дата створення зображення, ім'я автора, опис зображення, "
"або дані, призначені для читання комп'ютером, зокрема профіль кольорів ICC, "
"який визначає для комп'ютера спосіб, у який слід читати дані кольорів у "
"файлі."

#: ../../general_concepts/file_formats.rst:43
msgid "Openness"
msgstr "Відкритість"

#: ../../general_concepts/file_formats.rst:45
msgid ""
"This is a bit of an odd quality, but it's about how easy it to open or "
"recover the file, and how widely it's supported."
msgstr ""
"Це дещо нетипова якість. Її пов'язано із простотою відкриття або відновлення "
"даних у файлі та розповсюдженістю підтримки файлів певного формату."

#: ../../general_concepts/file_formats.rst:47
msgid ""
"Most internal file formats, like PSD are completely closed, and it's really "
"difficult for human outsiders to recover the data inside without opening "
"Photoshop. Other examples are camera raw files which have different "
"properties per camera manufacturer."
msgstr ""
"Більшість форматів для внутрішнього використання, подібних до PSD, є "
"повністю закритими. Наприклад, відновити дані у файлі PSD дуже складно, якщо "
"у вас немає встановленої відповідної версії Photoshop. Іншим прикладом таких "
"файлів є файли цифрових негативів (RAW) фотоапаратів, які мають у різних "
"виробників доволі різні властивості."

#: ../../general_concepts/file_formats.rst:49
msgid ""
"SVG, as a vector file format, is on the other end of the spectrum, and can "
"be opened with any text-editor and edited."
msgstr ""
"SVG, як формат файлів векторних даних, перебуває на іншому кінці спектра "
"відкритості. Його можна відкрити і редагувати у будь-якому текстовому "
"редакторі."

#: ../../general_concepts/file_formats.rst:51
msgid ""
"Most formats are in-between, and thus there's also a matter of how widely "
"supported the format is. JPG and PNG cannot be read or edited by human eyes, "
"but the vast majority of programs can open them, meaning the owner has easy "
"access to them."
msgstr ""
"Більшість форматів займають проміжне місце, тому вибір того чи іншого "
"формату також дуже впливає діапазон його підтримки у програмах. Людина не "
"здатна безпосередньо читати чи редагувати зображення у файлах JPG і PNG, але "
"існує безліч програм, які здатні відкривати ці файли, а це означає, що "
"власник таких файлів не матиме проблем із доступом до даних зображення."

#: ../../general_concepts/file_formats.rst:53
msgid "Contents:"
msgstr "Зміст:"
