msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___dockers___pattern_docker.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/dockers/pattern_docker.rst:1
msgid "Overview of the pattern docker."
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:11
msgid "Patterns"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:16
msgid "Patterns Docker"
msgstr "图案"

#: ../../reference_manual/dockers/pattern_docker.rst:19
msgid ".. image:: images/dockers/Krita_Patterns_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:20
msgid ""
"This docker allows you to select the global pattern. Using the open-file "
"button you can import patterns. Some common shortcuts are the following:"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:22
msgid "|mouseright| a swatch will allow you to set tags."
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:23
msgid "|mouseleft| a swatch will allow you to set it as global pattern."
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:24
msgid ":kbd:`Ctrl + scroll` you can resize the swatch sizes."
msgstr ""
