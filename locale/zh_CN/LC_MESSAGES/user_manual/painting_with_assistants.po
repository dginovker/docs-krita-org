msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___painting_with_assistants.pot\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/assistants/Assistants_fish-eye_2_02.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:2
msgid "How to use the painting assistants in Krita to draw perspectives."
msgstr "如何在 Krita 里面使用绘画辅助尺工具来绘制几何形状和透视。"

#: ../../user_manual/painting_with_assistants.rst:12
msgid "Painting Assistants"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:17
msgid "Painting with Assistants"
msgstr "绘画辅助尺"

#: ../../user_manual/painting_with_assistants.rst:19
msgid ""
"The assistant system allows you to have a little help while drawing straight "
"lines or circles."
msgstr ""
"Krita 的绘画辅助尺工具可以在你绘制直线、圆形等几何图形，甚至透视关系时提供一"
"定的辅助。"

#: ../../user_manual/painting_with_assistants.rst:22
msgid ""
"They can function as a preview shape, or you can snap onto them with the "
"freehand brush tool. In the tool options of free hand brush, you can toggle :"
"guilabel:`Snap to Assistants` to turn on snapping."
msgstr ""
"你可以把它们用作目标形状的预览图，也可以把手绘笔刷等工具吸附到它们上面。要启"
"用笔刷吸附功能，可在手绘笔刷工具的工具选项里面勾选 :guilabel:`吸附到辅助尺"
"` 。"

#: ../../user_manual/painting_with_assistants.rst:30
msgid ""
".. image:: images/assistants/Krita_basic_assistants.png\n"
"   :alt: Krita's vanishing point assistants in action"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:30
msgid "Krita's vanishing point assistants in action"
msgstr "Krita 的消失点辅助尺正在工作"

#: ../../user_manual/painting_with_assistants.rst:32
msgid "The following assistants are available in Krita:"
msgstr "接下来我们将介绍 Krita 提供的各种绘画辅助尺。"

#: ../../user_manual/painting_with_assistants.rst:35
msgid "Types"
msgstr "辅助尺类型"

#: ../../user_manual/painting_with_assistants.rst:37
msgid ""
"There are several types in Krita. You can select a type of assistant via the "
"tool options docker."
msgstr ""
"Krita 的绘画辅助尺工具可以创建几种不同的形状，你可以在它的工具选项面板按需选"
"用。"

#: ../../user_manual/painting_with_assistants.rst:43
msgid "Ellipse"
msgstr "椭圆"

#: ../../user_manual/painting_with_assistants.rst:45
msgid "An assistant for drawing ellipses and circles."
msgstr "用来绘制椭圆和圆形的辅助尺。"

#: ../../user_manual/painting_with_assistants.rst:47
msgid ""
"This assistant consists of three points: the first two are the axis of the "
"ellipse, and the last one is to determine its width."
msgstr ""
"此辅助尺有三个控制点：前两个点控制椭圆的轴长，最后一个点控制椭圆的宽度。"

#: ../../user_manual/painting_with_assistants.rst:51
msgid ""
"The same an ellipse, but allows for making ellipses that are concentric to "
"each other."
msgstr ""
"和椭圆类似，但可以绘制多个相互同轴的椭圆。使用此辅助尺时，你只需先创建一个参"
"考椭圆，然后切换到笔刷工具，在工具选项中勾选“吸附到辅助尺”，即可画出与参考椭"
"圆同轴的新椭圆。"

#: ../../user_manual/painting_with_assistants.rst:52
msgid "Concentric Ellipse"
msgstr "同轴椭圆"

#: ../../user_manual/painting_with_assistants.rst:54
#: ../../user_manual/painting_with_assistants.rst:144
msgid ""
"If you press the :kbd:`Shift` key while holding the first two handles, they "
"will snap to perfectly horizontal or vertical lines. Press the :kbd:`Shift` "
"key while holding the third handle, and it'll snap to a perfect circle."
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:61
#: ../../user_manual/painting_with_assistants.rst:154
msgid "Perspective"
msgstr "透视"

#: ../../user_manual/painting_with_assistants.rst:63
msgid "This ruler takes four points and creates a perspective grid."
msgstr "此辅助尺通过四个点创建一个透视网格。"

#: ../../user_manual/painting_with_assistants.rst:65
msgid ""
"This grid can be used with the 'perspective' sensor, which can influence "
"brushes."
msgstr ""
"此辅助尺创建的网格可以和“透视”传感器配合使用，该传感器会影响笔刷的空间效果。"

#: ../../user_manual/painting_with_assistants.rst:68
msgid ""
"If you press the :kbd:`Shift` key while holding any of the corner handles, "
"they'll snap to one of the other corner handles, in sets."
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:74
#: ../../user_manual/painting_with_assistants.rst:78
msgid "Ruler"
msgstr "直线"

#: ../../user_manual/painting_with_assistants.rst:76
msgid "There are three assistants in this group:"
msgstr "这个组别里面有三种辅助尺："

#: ../../user_manual/painting_with_assistants.rst:79
msgid "Helps create a straight line between two points."
msgstr "辅助绘制两点之间的一条直线。"

#: ../../user_manual/painting_with_assistants.rst:81
msgid "Infinite Ruler"
msgstr "无限直线"

#: ../../user_manual/painting_with_assistants.rst:81
msgid ""
"Extrapolates a straight line beyond the two visible points on the canvas."
msgstr "辅助绘制穿过两点横贯画布的一条直线。"

#: ../../user_manual/painting_with_assistants.rst:84
msgid ""
"This ruler allows you to draw a line parallel to the line between the two "
"points anywhere on the canvas."
msgstr ""
"先创建一条参考直线，然后切换为笔刷工具，打开吸附功能，即可绘制该参考直线的平"
"行线。"

#: ../../user_manual/painting_with_assistants.rst:85
msgid "Parallel Ruler"
msgstr "平行直线"

#: ../../user_manual/painting_with_assistants.rst:87
msgid ""
"If you press the :kbd:`Shift` key while holding the first two handles, they "
"will snap to perfectly horizontal or vertical lines."
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:93
msgid "Spline"
msgstr "曲线"

#: ../../user_manual/painting_with_assistants.rst:95
msgid ""
"This assistant allows you to position and adjust four points to create a "
"cubic bezier curve. You can then draw along the curve, snapping your brush "
"stroke directly to the curve line. Perfect curves every time!"
msgstr ""
"此辅助尺通过四个控制点画出一条贝塞尔曲线，你可以把笔刷吸附到曲线上，方便绘制"
"路径精确的曲线。"

#: ../../user_manual/painting_with_assistants.rst:99
msgid ""
"If you press the :kbd:`Shift` key while holding the first two handles, they "
"will snap to perfectly horizontal or vertical lines. Press the :kbd:`Shift` "
"key while holding the third or fourth handle, they will snap relative to the "
"handle they are attached to."
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:107
msgid "Vanishing Point"
msgstr "消失点"

#: ../../user_manual/painting_with_assistants.rst:109
msgid ""
"This assistant allows you to create a vanishing point, typically used for a "
"horizon line. A preview line is drawn and all your snapped lines are drawn "
"to this line."
msgstr ""
"此辅助尺可以创建一个消失点，通常放在地平线上。放置消失点后切换至笔刷工具，辅"
"助尺将为你显示透视预览线，你绘制的笔画也会被吸附到预览线上。"

#: ../../user_manual/painting_with_assistants.rst:112
msgid ""
"It is one point, with four helper points to align it to previously created "
"perspective lines."
msgstr ""
"每个辅助尺代表一个消失点，它还带有 4 个控制点用来对齐之前创建的透视线。"

#: ../../user_manual/painting_with_assistants.rst:115
msgid "They are made and manipulated with the :ref:`assistant_tool`."
msgstr "你可以用 :ref:`assistant_tool` 来创建和修改这些消失点。"

#: ../../user_manual/painting_with_assistants.rst:117
msgid ""
"If you press the :kbd:`Shift` key while holding the center handle, they will "
"snap to perfectly horizontal or vertical lines depending on the position of "
"where it previously was."
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:123
msgid "The vanishing point assistant also shows several general lines."
msgstr "消失点辅助尺可以显示额外的参考线。"

#: ../../user_manual/painting_with_assistants.rst:125
msgid ""
"When you've just created, or when you've just moved a vanishing point "
"assistant, it will be selected. This means you can modify the amount of "
"lines shown in the tool options of the :ref:`assistant_tool`."
msgstr ""
"当你选中一个消失点辅助尺时，可以在工具选项面板里修改 :ref:`assistant_tool` 显"
"示的参考线数量。"

#: ../../user_manual/painting_with_assistants.rst:130
msgid "Fish Eye Point"
msgstr "鱼眼点"

#: ../../user_manual/painting_with_assistants.rst:132
msgid ""
"Like the vanishing point assistant, this assistant is per a set of parallel "
"lines in a 3d space. So to use it effectively, use two, where the second is "
"at a 90 degrees angle of the first, and add a vanishing point to the center "
"of both. Or combine one with a parallel ruler and a vanishing point, or even "
"one with two vanishing points. The possibilities are quite large."
msgstr ""
"和消失点辅助尺一样，此辅助尺会在一个立体空间中显示一组平行线。它有下面几种典"
"型用法：第一种，创建两个垂直且互相重合的鱼眼点辅助尺；第二种，在鱼眼点中间添"
"加一个消失点辅助尺；第三种，用一个鱼眼点、一条平行直线和一个消失点辅助尺配合"
"使用。你可以搭配其他辅助尺使用鱼眼点，大胆地进行实验吧！"

#: ../../user_manual/painting_with_assistants.rst:139
msgid ""
"This assistant will not just give feedback/snapping between the vanishing "
"points, but also give feedback to the relative left and right of the "
"assistant. This is so you can use it in edge-cases like panoramas with "
"relative ease."
msgstr ""
"此辅助尺不仅可以在它的两个消失点之间的位置提供反馈和吸附，还可以在它们的左右"
"两侧提供反馈和吸附，你可以利用这个特性来绘制全景透视。"

#: ../../user_manual/painting_with_assistants.rst:149
msgid "Tutorials"
msgstr "相关教程"

#: ../../user_manual/painting_with_assistants.rst:151
msgid ""
"Check out this in depth discussion and tutorial on https://www.youtube.com/"
"watch?v=OhEv2pw3EuI"
msgstr ""
"有关此功能的深入探讨和视频教程，可访问：https://www.youtube.com/watch?"
"v=OhEv2pw3EuI"

#: ../../user_manual/painting_with_assistants.rst:154
msgid "Technical Drawing"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:157
msgid "Setting up Krita for technical drawing-like perspectives"
msgstr "技术图纸透视设置范例"

#: ../../user_manual/painting_with_assistants.rst:159
msgid ""
"So now that you've seen the wide range of drawing assistants that Krita "
"offers, here is an example of how using these assistants you can set up "
"Krita for technical drawing."
msgstr ""
"我们在前面已经介绍了 Krita 提供的各种绘画辅助尺。接下来我们将示范如何使用它们"
"来绘制技术图纸风格的画面。"

#: ../../user_manual/painting_with_assistants.rst:163
msgid ""
"This tutorial below should give you an idea of how to set up the assistants "
"for specific types of technical views."
msgstr ""
"如果你想绘制某些类型的技术图纸风格画面，你可以在本教程找到对应的绘画辅助尺配"
"置方式。"

#: ../../user_manual/painting_with_assistants.rst:166
msgid ""
"If you want to instead do the true projection, check out :ref:`the "
"projection category <cat_projection>`."
msgstr ""
"如果你想要学习真正的透视投影原理，可参考 :ref:`透视投影原理分类文章 "
"<cat_projection>`。"

#: ../../user_manual/painting_with_assistants.rst:169
msgid "Orthographic"
msgstr "正交投影"

#: ../../user_manual/painting_with_assistants.rst:171
msgid ""
"Orthographic is a mode where you try to look at something from the left or "
"the front. Typically, you try to keep everything in exact scale with each "
"other, unlike perspective deformation."
msgstr ""
"正交投影是一种从物体的正面或者侧面进行观察的方式。在典型情况下你要保持前后物"
"体的比例相同，不要发生透视变形。"

#: ../../user_manual/painting_with_assistants.rst:175
msgid ""
"The key assistant you want to use here is the Parallel Ruler. You can set "
"these up horizontally or vertically, so you always have access to a Grid."
msgstr ""
"这种扁平的图纸主要使用平行直线辅助尺。在水平方向和垂直方向各放置一组平行线，"
"即可形成笔刷可以吸附的平面网格。"

#: ../../user_manual/painting_with_assistants.rst:180
msgid "Axonometric"
msgstr "轴测图"

#: ../../user_manual/painting_with_assistants.rst:182
msgid "All of these are set up using three Parallel Rulers."
msgstr "轴测图有许多种，但都可以通过平行直线辅助尺实现。"

#: ../../user_manual/painting_with_assistants.rst:185
msgid ".. image:: images/assistants/Assistants_oblique.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:187
msgid ""
"For oblique, set two parallel rulers to horizontal and vertical, and one to "
"an angle, representing depth."
msgstr ""
"如需制作斜轴测图，可先在水平方向和垂直方向上各放置一个平行直线辅助尺，然后在"
"倾斜方向上再放置一个平行直线辅助尺来代表深度。"

#: ../../user_manual/painting_with_assistants.rst:188
msgid "Oblique"
msgstr "斜轴测"

#: ../../user_manual/painting_with_assistants.rst:191
msgid ".. image:: images/assistants/Assistants_dimetric.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:193
msgid ""
"Isometric perspective has technically all three rulers set up at 120° from "
"each other. Except when it's game isometric, then it's a type of dimetric "
"projection where the diagonal values are a 116.565° from the main. The "
"latter can be easily set up by snapping the assistants to a grid."
msgstr ""
"等轴测透视就是把斜轴测的三条平行直线辅助尺以 120° 的夹角摆放。游戏常用的二轴"
"测透视在这个基础上有一点变化，即两条斜线和垂直线之间的夹角为 116.565°，这可以"
"通过把辅助尺吸附到网格来实现。"

#: ../../user_manual/painting_with_assistants.rst:197
msgid "Dimetric & Isometric"
msgstr "二轴测和等轴测"

#: ../../user_manual/painting_with_assistants.rst:200
msgid ".. image:: images/assistants/Assistants_trimetric.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:202
msgid ""
"Is when all the angles are slightly different. Often looks like a slightly "
"angled isometric."
msgstr ""
"在此透视法下，三条参考线之间的夹角都不一样。可以把它看作是稍微倾斜的等轴测透"
"视。"

#: ../../user_manual/painting_with_assistants.rst:203
msgid "Trimetric"
msgstr "三轴测"

#: ../../user_manual/painting_with_assistants.rst:206
msgid "Linear Perspective"
msgstr "线性透视"

#: ../../user_manual/painting_with_assistants.rst:209
msgid ".. image:: images/assistants/Assistants_1_point_perspective.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:211
msgid ""
"A 1 point perspective is set up using 1 vanishing point, and two crossing "
"perpendicular parallel rulers."
msgstr ""
"要组建单点透视，先在画面中间放置一个消失点辅助尺，然后在水平和垂直方向上各放"
"置一条平行直线辅助尺。"

#: ../../user_manual/painting_with_assistants.rst:212
msgid "1 Point Perspective"
msgstr "单点透视"

#: ../../user_manual/painting_with_assistants.rst:215
msgid ".. image:: images/assistants/Assistants_2_point_perspective.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:217
msgid ""
"A 2 point perspective is set up using 2 vanishing point and 1 vertical "
"parallel ruler. Often, putting the vanishing points outside the frame a "
"little can decrease the strength of it."
msgstr ""
"要组建二点透视，可在画面两侧各放置一个消失点辅助尺，然后在垂直方向上放置一条"
"平行直线辅助尺。两个消失点之间的距离越远，透视的强烈程度也就越小。"

#: ../../user_manual/painting_with_assistants.rst:219
msgid "2 Point Perspective"
msgstr "二点透视"

#: ../../user_manual/painting_with_assistants.rst:222
msgid ".. image:: images/assistants/Assistants_2_pointperspective_02.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:224
msgid ".. image:: images/assistants/Assistants_3_point_perspective.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:226
msgid "3 Point Perspective"
msgstr "三点透视"

#: ../../user_manual/painting_with_assistants.rst:226
msgid "A 3 point perspective is set up using 3 vanishing point rulers."
msgstr "三点透视通过三个消失点辅助尺进行构建。"

#: ../../user_manual/painting_with_assistants.rst:229
msgid "Logic of the vanishing point"
msgstr "消失点的原理和用法"

#: ../../user_manual/painting_with_assistants.rst:231
msgid ""
"There's a little secret that perspective tutorials don't always tell you, "
"and that's that a vanishing point is the point where any two parallel lines "
"meet. This means that a 1 point perspective and 2 point perspective are "
"virtually the same."
msgstr ""
"一个消失点是空间中两条平行线交汇的点。这是一个非常有用的知识点，但许多透视教"
"程往往会忽略它。它意味着单点透视和二点透视实际上是一回事。(注意：由于原文省略"
"了太多步骤，译者并没有真正领会最后这句话的意思，译文已经按照标准二点透视原理"
"尽可能地补充了缺少的步骤)"

#: ../../user_manual/painting_with_assistants.rst:233
msgid ""
"We can prove this via a little experiment. That good old problem: drawing a "
"rail-road."
msgstr ""
"我们可以通过一个小实验来证明这种说法。这个实验便是我们永恒的课题：画一条通向"
"地平线的铁路。"

#: ../../user_manual/painting_with_assistants.rst:236
msgid ".. image:: images/assistants/Assistants_vanishing_point_logic_01.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:237
msgid ""
"You are probably familiar with the problem: How to determine where the next "
"beam is going to be, as perspective projection will make them look closer "
"together."
msgstr ""
"你可能对将要遇到的困难并不陌生：怎么确定下一根枕木的位置呢？透视原理决定了它"
"们离得越远，挤得越近。你当然可以依靠直觉来描绘，但怎么画才能得到精确的效果"
"呢？"

#: ../../user_manual/painting_with_assistants.rst:240
msgid ""
"Typically, the solution is to draw a line in the middle and then draw lines "
"diagonally across. After all, those lines are parallel, meaning that the "
"exact same distance is used."
msgstr ""
"一般来说，解决方案是画面左侧外部的地平线上建立一个消失点，从该消失点引出一条"
"斜线，连接到到第一根枕木与右侧铁轨的交点。然后在两条铁轨位于地平线的交汇点上"
"画一根垂直线，把它与之前所画的斜线相交在一点。最后通过这个点画一根水平线。这"
"条水平线便是第二根枕木所在的位置了。后面的枕木如此类推。"

#: ../../user_manual/painting_with_assistants.rst:243
msgid ".. image:: images/assistants/Assistants_vanishing_point_logic_02.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:244
msgid ""
"But because they are parallel, we can use a vanishing point assistant "
"instead, and we use the alignment handles to align it to the diagonal of the "
"beam, and to the horizontal (here marked with red)."
msgstr ""
"不过因为枕木是平行的，所以我们可以使用消失点辅助尺的对齐线功能。把其中一条对"
"齐线的两个控制点各自放在枕木在对角线上的两个点上，把另一条对齐线放在地平线的"
"铁轨交点 (如图红线所示)。"

#: ../../user_manual/painting_with_assistants.rst:247
msgid ""
"That diagonal can then in turn be used to determine the position of the "
"beams:"
msgstr "这样我们便利用了横穿第一根枕木对角线的斜线来确定其他枕木的位置："

#: ../../user_manual/painting_with_assistants.rst:251
msgid ".. image:: images/assistants/Assistants_vanishing_point_logic_03.png"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:252
msgid ""
"Because any given set of lines has a vanishing point (outside of the ones "
"flat on the view-plane), there can be an infinite amount of vanishing points "
"in a linear perspective. Therefore, Krita allows you to set vanishing points "
"yourself instead of forcing you to only use a few."
msgstr ""
"因为除了平行于地平线之外的每条线都会指向一个消失点，所以在线性透视中实际上可"
"以有无限多个消失点，Krita 也因此允许你自由设定消失点的位置。"

#: ../../user_manual/painting_with_assistants.rst:255
msgid "Fish Eye perspective"
msgstr "鱼眼透视"

#: ../../user_manual/painting_with_assistants.rst:257
msgid ""
"Fish eye perspective works much the same as the linear perspective, the big "
"difference being that in a fish-eye perspective, any parallel set of lines "
"has two vanishing points, each for one side."
msgstr ""
"鱼眼透视和线性透视的工作原理大体上是一致的，最大的不同是在鱼眼透视里面任何平"
"行的线条都具有两个消失点，一边一个。"

#: ../../user_manual/painting_with_assistants.rst:261
msgid ""
"So, to set them up, the easiest way is one horizontal, one vertical, on the "
"same spot, and one vanishing point assistant in the middle."
msgstr ""
"要构建一组鱼眼透视，最简单的办法就是在水平和垂直方向上建立两个位置重合的鱼眼"
"点辅助尺，然后把一个消失点辅助尺放置在它们的正中间。"

#: ../../user_manual/painting_with_assistants.rst:265
msgid ".. image:: images/assistants/Fish-eye.gif"
msgstr ""

#: ../../user_manual/painting_with_assistants.rst:266
msgid ""
"But, you can also make one horizontal one that is just as big as the other "
"horizontal one, and put it halfway:"
msgstr "你还可以把两个水平方向的鱼眼点辅助尺以画面中间线为界并排放置："
