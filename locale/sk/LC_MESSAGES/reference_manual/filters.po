# translation of docs_krita_org_reference_manual___filters.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-13 10:24+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/filters.rst:5
msgid "Filters"
msgstr "Filtre"

#: ../../reference_manual/filters.rst:7
msgid ""
"Filters are little scripts or operations you can run on your drawing. You "
"can visualize them as real-world camera filters that can make a photo darker "
"or blurrier. Or perhaps like a coffee filter, where only water and coffee "
"gets through, and the ground coffee stays behind."
msgstr ""

#: ../../reference_manual/filters.rst:9
msgid ""
"Filters are unique to digital painting in terms of complexity, and their "
"part of the painting pipeline. Some artists only use filters to adjust their "
"colors a little. Others, using Filter Layers and Filter Masks use them to "
"dynamically update a part of an image to be filtered. This way, they can "
"keep the original underneath without changing the original image. This is a "
"part of a technique called 'non-destructive' editing."
msgstr ""

#: ../../reference_manual/filters.rst:11
msgid ""
"Filters can be accessed via the :guilabel:`Filters` menu. Krita has two "
"types of filters: Internal and G'MIC filters."
msgstr ""

#: ../../reference_manual/filters.rst:13
msgid ""
"Internal filters are often multithreaded, and can thus be used with the "
"filter brush or the adjustment filters."
msgstr ""
