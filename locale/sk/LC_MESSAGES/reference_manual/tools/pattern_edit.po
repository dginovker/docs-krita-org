# translation of docs_krita_org_reference_manual___tools___pattern_edit.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___pattern_edit\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-04-02 13:44+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:20
msgid ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: toolpatternedit"
msgstr ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: toolpatternedit"

#: ../../reference_manual/tools/pattern_edit.rst:1
msgid "Krita's vector pattern editing tool reference."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:11
#, fuzzy
#| msgid "Pattern Size:"
msgid "Pattern"
msgstr "Veľkosť vzoru:"

#: ../../reference_manual/tools/pattern_edit.rst:16
msgid "Pattern Editing Tool"
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:18
msgid "|toolpatternedit|"
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:22
msgid ""
"The pattern editing tool has been removed in 4.0, currently there's no way "
"to edit pattern fills for vectors."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:24
msgid ""
"The Pattern editing tool works on Vector Shapes that use a Pattern fill. On "
"these shapes, the Pattern Editing Tool allows you to change the size, ratio "
"and origin of a pattern."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:27
msgid "On Canvas-editing"
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:29
msgid ""
"You can change the origin by click dragging the upper node, this is only "
"possible in Tiled mode."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:31
msgid ""
"You can change the size and ratio by click-dragging the lower node. There's "
"no way to constrain the ratio in on-canvas editing, this is only possible in "
"Original and Tiled mode."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:34
msgid "Tool Options"
msgstr "Voľby nástroja"

#: ../../reference_manual/tools/pattern_edit.rst:36
msgid "There are several tool options with this tool, for fine-tuning:"
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:38
msgid "First there are the Pattern options."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:41
msgid "This can be set to:"
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:43
msgid "Original:"
msgstr "Originál:"

#: ../../reference_manual/tools/pattern_edit.rst:44
msgid "This will only show one, unstretched, copy of the pattern."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:45
msgid "Tiled (Default):"
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:46
msgid "This will let the pattern appear tiled in the x and y direction."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Repeat:"
msgstr "Opakovanie:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Stretch:"
msgstr "Natiahnuť:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "This will stretch the Pattern image to the shape."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:51
msgid "Pattern origin. This can be set to:"
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:53
msgid "Top-left"
msgstr "Vľavo hore"

#: ../../reference_manual/tools/pattern_edit.rst:54
msgid "Top"
msgstr "Hore"

#: ../../reference_manual/tools/pattern_edit.rst:55
msgid "Top-right"
msgstr "Vpravo hore"

#: ../../reference_manual/tools/pattern_edit.rst:56
msgid "Left"
msgstr "Vľavo"

#: ../../reference_manual/tools/pattern_edit.rst:57
msgid "Center"
msgstr "Vycentrovať"

#: ../../reference_manual/tools/pattern_edit.rst:58
msgid "Right"
msgstr "Vpravo"

#: ../../reference_manual/tools/pattern_edit.rst:59
msgid "Bottom-left"
msgstr "Vľavo dole"

#: ../../reference_manual/tools/pattern_edit.rst:60
msgid "Bottom"
msgstr "Dole"

#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Reference point:"
msgstr "Referenčný bod:"

#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Bottom-right."
msgstr "Vpravo dole"

#: ../../reference_manual/tools/pattern_edit.rst:64
msgid "For extra tweaking, set in percentages."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:66
msgid "X:"
msgstr "X:"

#: ../../reference_manual/tools/pattern_edit.rst:67
msgid "Offset in the X coordinate, so horizontally."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Reference Point Offset:"
msgstr "Posun referenčného bodu:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Y:"
msgstr "Y:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Offset in the Y coordinate, so vertically."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:71
msgid "Tile Offset:"
msgstr "Posun dlaždice:"

#: ../../reference_manual/tools/pattern_edit.rst:72
msgid "The tile offset if the pattern is tiled."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:74
msgid "Fine Tune the resizing of the pattern."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:76
msgid "W:"
msgstr "W:"

#: ../../reference_manual/tools/pattern_edit.rst:77
msgid "The width, in pixels."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "Pattern Size:"
msgstr "Veľkosť vzoru:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "H:"
msgstr "H:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "The height, in pixels."
msgstr ""

#: ../../reference_manual/tools/pattern_edit.rst:81
msgid ""
"And then there's :guilabel:`Patterns`, which is a mini pattern docker, and "
"where you can pick the pattern used for the fill."
msgstr ""
