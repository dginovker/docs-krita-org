# translation of docs_krita_org_reference_manual___clones_array.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___clones_array\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-13 13:15+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Angle"
msgstr "Uhol"

#: ../../reference_manual/clones_array.rst:1
msgid "The Clones Array functionality in Krita"
msgstr ""

#: ../../reference_manual/clones_array.rst:10
#: ../../reference_manual/clones_array.rst:15
msgid "Clones Array"
msgstr "Klonuje pole"

#: ../../reference_manual/clones_array.rst:10
msgid "Clone"
msgstr "Klonovať"

#: ../../reference_manual/clones_array.rst:17
msgid ""
"Allows you to create a set of clone layers quickly. These are ordered in "
"terms of rows and columns. The default options will create a 2 by 2 grid. "
"For setting up tiles of an isometric game, for example, you'd want to set "
"the X offset of the rows to half the value input into the X offset for the "
"columns, so that rows are offset by half. For a hexagonal grid, you'd want "
"to do the same, but also reduce the Y offset of the grids by the amount of "
"space the hexagon can overlap with itself when tiled."
msgstr ""

#: ../../reference_manual/clones_array.rst:19
msgid "\\- Elements"
msgstr "\\- Prvky"

#: ../../reference_manual/clones_array.rst:20
msgid ""
"The amount of elements that should be generated using a negative of the "
"offset."
msgstr ""

#: ../../reference_manual/clones_array.rst:21
msgid "\\+ Elements"
msgstr "\\+ Prvky"

#: ../../reference_manual/clones_array.rst:22
msgid ""
"The amount of elements that should be generated using a positive of the "
"offset."
msgstr ""

#: ../../reference_manual/clones_array.rst:23
msgid "X offset"
msgstr "Posun X"

#: ../../reference_manual/clones_array.rst:24
msgid ""
"The X offset in pixels. Use this in combination with Y offset to position a "
"clone using Cartesian coordinates."
msgstr ""

#: ../../reference_manual/clones_array.rst:25
msgid "Y offset"
msgstr "Posun Y"

#: ../../reference_manual/clones_array.rst:26
msgid ""
"The Y offset in pixels. Use this in combination with X offset to position a "
"clone using Cartesian coordinates."
msgstr ""

#: ../../reference_manual/clones_array.rst:27
msgid "Distance"
msgstr "Vzdialenosť"

#: ../../reference_manual/clones_array.rst:28
msgid ""
"The line-distance of the original origin to the clones origin. Use this in "
"combination with angle to position a clone using a polar coordinate system."
msgstr ""

#: ../../reference_manual/clones_array.rst:30
msgid ""
"The angle-offset of the column or row. Use this in combination with distance "
"to position a clone using a polar coordinate system."
msgstr ""
