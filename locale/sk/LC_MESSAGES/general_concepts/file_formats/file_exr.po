# translation of docs_krita_org_general_concepts___file_formats___file_exr.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_exr\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-01 13:36+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../general_concepts/file_formats/file_exr.rst:1
msgid "The EXR file format as exported by Krita."
msgstr "Súborový formát EXR ako exportovaný z Krita."

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "EXR"
msgstr ""

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "HDR Fileformat"
msgstr ""

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "OpenEXR"
msgstr ""

#: ../../general_concepts/file_formats/file_exr.rst:10
#, fuzzy
#| msgid "\\*.exr"
msgid "*.exr"
msgstr "\\*.exr"

#: ../../general_concepts/file_formats/file_exr.rst:15
msgid "\\*.exr"
msgstr "\\*.exr"

#: ../../general_concepts/file_formats/file_exr.rst:17
msgid ""
"``.exr`` is the prime file format for saving and loading :ref:`floating "
"point bit depths <bit_depth>`, and due to the library made to load and save "
"these images being fully open source, the main interchange format as well."
msgstr ""

#: ../../general_concepts/file_formats/file_exr.rst:19
msgid ""
"Floating point bit-depths are used by the computer graphics industry to "
"record scene referred values, which can be made via a camera or a computer "
"renderer. Scene referred values means that the file can have values whiter "
"than white, which in turn means that such a file can record lighting "
"conditions, such as sunsets very accurately. These EXR files can then be "
"used inside a renderer to create realistic lighting."
msgstr ""

#: ../../general_concepts/file_formats/file_exr.rst:21
msgid ""
"Krita can load and save EXR for the purpose of paint-over (yes, Krita can "
"paint with scene referred values) and interchange with applications like "
"Blender, Mari, Nuke and Natron."
msgstr ""
