msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/filters/enhance.rst:1
msgid "Overview of the enhance filters."
msgstr ""

#: ../../reference_manual/filters/enhance.rst:10
#: ../../reference_manual/filters/enhance.rst:19
msgid "Sharpen"
msgstr ""

#: ../../reference_manual/filters/enhance.rst:10
msgid "Filters"
msgstr ""

#: ../../reference_manual/filters/enhance.rst:15
msgid "Enhance"
msgstr ""

#: ../../reference_manual/filters/enhance.rst:17
msgid ""
"These filters all focus on reducing the blur in the image by sharpening and "
"enhancing details and the edges. Following are various sharpen and enhance "
"filters in provided in Krita."
msgstr ""

#: ../../reference_manual/filters/enhance.rst:20
msgid "Mean Removal"
msgstr ""

#: ../../reference_manual/filters/enhance.rst:21
msgid "Unsharp Mask"
msgstr ""

#: ../../reference_manual/filters/enhance.rst:22
msgid "Gaussian Noise reduction"
msgstr ""

#: ../../reference_manual/filters/enhance.rst:23
msgid "Wavelet Noise Reducer"
msgstr ""
