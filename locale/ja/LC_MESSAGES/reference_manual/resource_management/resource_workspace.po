msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/resource_management/resource_workspace.rst:1
msgid "Managing workspaces and sessions in Krita."
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:16
msgid "Workspaces"
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:25
msgid "Window Layouts"
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:37
msgid "Sessions"
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:11
msgid "Resources"
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:18
msgid ""
"Workspaces are basically saved configurations of dockers.  Each workspace "
"saves how the dockers are grouped and where they are placed on the screen.  "
"They allow you to easily move between workflows without having to manual "
"reconfigure your setup each time.  They can be as simple or as complex as "
"you want."
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:20
msgid ""
"Workspaces can only be accessed via the toolbar or :menuselection:`Window --"
"> Workspaces` There's no docker for them.  You can save workspaces, in which "
"your current configuration is saved. You can also import them (from a \\*."
"kws file), or delete them (which black lists them)."
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:22
msgid ""
"Workspaces can technically be tagged, but outside of the resource manager "
"this is not possible."
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:27
msgid ""
"When you work with multiple screens, a single window with a single workspace "
"won't be enough. For multi monitor setups we instead can use sessions. "
"Window layouts allow us to store multiple windows, their positions and the "
"monitor they were on."
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:29
msgid ""
"You can access Window Layouts from the workspace drop-down in the toolbar."
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:31
msgid "Primary Workspace Follows Focus"
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:32
msgid ""
"This treats the workspace in the first window as the 'primary' workspace, "
"and when you switch focus, it will switch the secondary windows to that "
"primary workspace. This is useful when the secondary workspace is a very "
"sparse workspace with few dockers, and the primary is one with a lot of "
"different dockers."
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:34
msgid "Show Active Image In All Windows"
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:34
msgid ""
"This will synchronise the currently viewed image in all windows. Without it, "
"different windows can open separate views for an image via :menuselection:"
"`Window --> New View --> document.kra`."
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:39
msgid ""
"Sessions allow Krita to store the images and windows opened. You can tell "
"Krita to automatically save current or recover previous sessions if so "
"configured in the :ref:`misc_settings`."
msgstr ""

#: ../../reference_manual/resource_management/resource_workspace.rst:41
msgid "You can access sessions from :menuselection:`File --> Sessions`."
msgstr ""
