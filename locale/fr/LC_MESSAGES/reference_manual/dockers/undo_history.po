msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-27 02:40+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<generated>:1
msgid "Split strokes."
msgstr ""

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:1
msgid "Overview of the undo history docker."
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:11
msgid "Undo"
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:11
msgid "Redo"
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:11
msgid "History"
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:16
msgid "Undo History"
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:19
msgid ".. image:: images/dockers/Krita_Undo_History_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:20
msgid ""
"This docker allows you to quickly shift between undo states, and even go "
"back in time far more quickly that rapidly reusing the :kbd:`Ctrl + Z` "
"shortcut."
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:22
msgid "Cumulate Undo"
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:25
msgid "Cumulative Undo"
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:27
msgid ""
"|mouseright| an item in the undo-history docker to enable cumulative undo. |"
"mouseright| again to change the parameters:"
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:29
msgid "Start merging time"
msgstr "Délai avant cumulation"

#: ../../reference_manual/dockers/undo_history.rst:30
msgid ""
"The amount of seconds required to consider a group of strokes to be worth "
"one undo step."
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:31
msgid "Group time"
msgstr "Délai de regroupement"

#: ../../reference_manual/dockers/undo_history.rst:32
msgid ""
"According to this parameter -- groups are made. Every stroke is put into the "
"same group till two consecutive strokes have a time gap of more than T "
"seconds. Then a new group is started."
msgstr ""

#: ../../reference_manual/dockers/undo_history.rst:34
msgid ""
"A user may want to keep the ability of Undoing/Redoing his last N strokes. "
"Once N is crossed -- the earlier strokes are merged into the group's first "
"stroke."
msgstr ""
