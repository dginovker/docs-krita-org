# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-19 23:31+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: zague icons zigue image images curvebrush\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-4.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-4.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:1
msgid "The Curve Brush Engine manual page."
msgstr "A página de manual do Motor de Pincéis de Curvas."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:13
#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:18
msgid "Curve Brush Engine"
msgstr "Motor de Pincéis de Curvas"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:13
msgid "Brush Engine"
msgstr "Motor de Pincéis"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:21
msgid ".. image:: images/icons/curvebrush.svg"
msgstr ".. image:: images/icons/curvebrush.svg"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:22
msgid ""
"The curve brush is a brush engine which creates strokes made of evenly "
"spaced lines. It has, among other things been used as a replacement for "
"pressure sensitive strokes in lieu of a tablet."
msgstr ""
"O pincel de curvas é um motor de pincéis que cria traços com base em linhas "
"espaçadas de forma homogénea. Tem, entre outras coisas, sido usado como um "
"substituto para os traços sensíveis à pressão no lugar de uma tablete."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:25
msgid "Settings"
msgstr "Configuração"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:27
msgid ""
"First off, the line produced by the Curve brush is made up of 2 sections:"
msgstr ""
"Primeiro que tudo, a linha produzida pelo pincel de Curvas é composto por 2 "
"secções:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:29
msgid "The connection line, which is the main line drawn by your mouse"
msgstr "A linha de ligação, que é a linha principal desenhada pelo seu rato"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:31
msgid ""
"The curve lines I think, which are the extra fancy lines that form at "
"curves. The curve lines are formed by connecting one point of the curve to a "
"point earlier on the curve. This also means that if you are drawing a "
"straight line, these lines won't be visible, since they'll overlap with the "
"connection line. Drawing faster gives you wider curves areas."
msgstr ""
"As linhas das curvas são as linhas bonitas extra que se formam com base nas "
"curvas. As linhas das curvas são compostas pela ligação de um ponto da curva "
"a um ponto anterior da mesma. Isto também significa que, se estiver a "
"desenhar uma linha recta, estas linhas não serão visíveis, dado que ficarão "
"todas sobrepostas com a linha de ligação. Se desenhar mais depressa, irá "
"obter áreas de curvas mais amplas."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:35
msgid ".. image:: images/brushes/Krita-tutorial6-I.1-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.1-1.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:36
msgid ""
"You have access to 3 settings from the Lines tab, as well as 2 corresponding "
"dynamics:"
msgstr ""
"Tem acesso a 3 opções na página de Linhas, assim como 2 dinâmicas "
"correspondentes:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:38
msgid ""
"Line width: this applies to both the connection line and the curve lines."
msgstr ""
"Espessura da linha: isto aplica-se tanto à linha de ligação como às linhas "
"da curva."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:40
msgid "Line width dynamics: use this to vary line width dynamically."
msgstr ""
"Dinâmica da espessura da linha: use isto para variar a espessura da linha de "
"forma dinâmica."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:42
msgid ""
"History size: this determines the distance for the formation of curve lines."
msgstr ""
"Tamanho do histórico: isto define a distância para a formação das linhas da "
"curva."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:44
msgid ""
"If you set this at low values, then the curve lines can only form over a "
"small distances, so they won't be too visible."
msgstr ""
"Se configurar isto com valores baixos, então as linhas da curva só se "
"poderão formar ao longo de distâncias curtas, pelo que não serão demasiado "
"visíveis."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:45
msgid ""
"On the other hand, if you set this value too high, the curve lines will only "
"start forming relatively \"late\"."
msgstr ""
"Por outro lado, se configurar este valor demasiado elevado, as linhas da "
"curva só se irão começar a formar relativamente \"tarde\"."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:46
msgid ""
"So in fact, you'll get maximum curve lines area with a mid-value of say... "
"40~60, which is about the default value. Unless you're drawing at really "
"high resolutions."
msgstr ""
"Como tal, de facto, irá obter a área máxima das linhas da curva com um valor "
"intermédio aproximado a... 40~60, que é aproximado ao valor predefinido. A "
"menos que esteja a desenhar com resoluções realmente elevadas."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:48
msgid ""
"Curves opacity: you can't set different line widths for the connection line "
"and the curve lines, but you can set a different opacity for the curve "
"lines. With low opacity, this will produce the illusion of thinner curve "
"lines."
msgstr ""
"Opacidade das curvas: não poderá definir espessuras de linhas diferentes "
"para a linha de ligação e para as linhas da curva, mas poderá definir uma "
"opacidade diferente para as linhas da curva. Com uma baixa opacidade, irá "
"produzir a ilusão de linhas da curva mais finas."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:50
msgid "Curves opacity dynamics: use this to vary Curves opacity dynamically."
msgstr ""
"Dinâmica da opacidade das curvas: use isto para variar a opacidade das "
"curvas de forma dinâmica."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:52
msgid "In addition, you have access to two checkboxes:"
msgstr "Para além disso, tem acesso a duas opções de marcação:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:54
msgid ""
"Paint connection line, which toggles the visibility of the connection line"
msgstr ""
"Pintar a linha de ligação, que activa ou desactiva a visibilidade da linha "
"de ligação"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:55
msgid ""
"Smoothing, which... I have no idea actually. I don't see any differences "
"with or without it. Maybe it's for tablets?"
msgstr ""
"Suavização, que... de facto, parece não fazer grande diferença com ou sem "
"ela. Talvez sirva para as tabletes?"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:58
msgid ".. image:: images/brushes/Krita-tutorial6-I.1-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.1-2.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:60
msgid "Drawing variable-width lines"
msgstr "Desenhar linhas de espessura variável"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:62
msgid ""
"And here's the only section of this tutorial that anyone cares about: pretty "
"lineart lines! For this:"
msgstr ""
"E aqui está a única do tutorial que realmente interessa a alguém: linhas "
"artísticas bonitas! Para isto:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:64
msgid ""
"Use the Draw Dynamically mode: I tend to increase drag to at least 50. Vary "
"Mass and Drag until you get the feel that's most comfortable for you."
msgstr ""
"Use o modo para Desenhar Dinamicamente: o autor tende a aumentar o "
"arrastamento para pelo menos 50. Varie a Massa e o Arrastamento até que "
"tenha a melhor sensação para si."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:67
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-1.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:68
msgid ""
"Set line width to a higher value (ex.: 5), then turn line width dynamics on:"
msgstr ""
"Configure a espessura da linha para um valor maior (ex.: 5), activando então "
"a dinâmica da espessura da linha:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:70
msgid ""
"If you're a tablet user, just set this to Pressure (this should be selected "
"by default so just turn on the Line Width dynamics). I can't check myself, "
"but a tablet user confirmed to me that it works well enough with Draw "
"Dynamically."
msgstr ""
"Se for um utilizador de tablete, basta configurar isto com a Pressão (deverá "
"estar seleccionada por omissão, por isso basta ligar a Dinâmica da Espessura "
"da Linha). Não é possível ao autor validar, mas um utilizador de tablete "
"confirmou que funciona suficientemente bem com a opção Desenhar "
"Dinamicamente."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:71
msgid ""
"If you're a mouse user hoping to get variable line width, set the Line Width "
"dynamics to Speed."
msgstr ""
"Se for um utilizador com o rato que espera obter uma espessura de linha "
"variável, configure a Dinâmica da Espessura da Linha com a Velocidade."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:74
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-2.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:75
msgid ""
"Set Curves opacity to 0: This is the simplest way to turn off the Curve "
"lines. That said, leaving them on will get you more \"expressive\" lines."
msgstr ""
"Configure a opacidade das Curvas como 0: Esta é a forma mais simples de "
"desligar as linhas da Curva. Posto isto, se as deixar ligadas, irá obter "
"linhas mais \"expressivas\"."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:78
msgid "Additional tips:"
msgstr "Dicas adicionais:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:80
msgid "Zig-zag a lot if you want a lot of extra curves lines."
msgstr "Faça um pouco de zigue-zague se quiser ter bastantes linhas da curva."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:81
msgid ""
"Use smooth, sweeping motions when you're using Draw Dynamically with Line "
"Width set to Speed: abrupt speed transitions will cause abrupt size "
"transitions. It takes a bit of practice, and the thicker the line, the more "
"visible the deformities will be. Also, zoom in to increase control."
msgstr ""
"Use movimentos suaves de varredura quando estiver a usar a opção Desenhar "
"Dinamicamente com a Espessura da Linha configurada com a Velocidade: as "
"transições abruptas de velocidade provocarão transições de tamanho "
"igualmente abruptas. Exige alguma prática e, quanto mais grossa for a linha, "
"mais visíveis serão as deformações. Do mesmo modo, amplie para aumentar o "
"controlo."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:82
msgid ""
"If you need to vary between thin and thick lines, I suggest creating presets "
"of different widths, since you can't vary the base line width from the "
"canvas."
msgstr ""
"Se precisar de alternar entre linhas finas e grossas, sugerimos que crie "
"predefinições com espessuras diferentes, dado que não consegue variar a "
"espessura de base da linha a partir da área de desenho."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:84
msgid "Alternative:"
msgstr "Alternativa:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:86
msgid "Use the Draw Dynamically mode"
msgstr "Use o modo para Desenhar Dinamicamente"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:87
msgid "Set Curves opacity to 100"
msgstr "Configure a opacidade das Curvas como 100"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:88
msgid "Optionally decrease History size to about 30"
msgstr "Opcionalmente, reduza o Tamanho do Histórico para cerca de 30"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:90
msgid ""
"The curve lines will fill out the area they cover completely, resulting in a "
"line with variable widths. Anyway, here are some comparisons:"
msgstr ""
"As linhas da curva irão preencher a área que cobrem por completo, resultando "
"numa linha com espessuras variáveis. De qualquer forma, existem algumas "
"comparações:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:93
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-3.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-3.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:94
msgid "And here are examples of what you can do with this brush:"
msgstr "E aqui estão exemplos do que poderá fazer com este pincel:"
