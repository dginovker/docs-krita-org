# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:21+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en toolshapeedit guilabel image Kritamouseleft kbd\n"
"X-POFile-SpellExtra: rectangletool pathtool images alt toolpolygon\n"
"X-POFile-SpellExtra: ellipsetool linetool toolpolyline icons shapeedittool\n"
"X-POFile-SpellExtra: toolfreehandpath toolline bézier beziercurve ref\n"
"X-POFile-SpellExtra: polygontool toolrectangle Bézier tool toolellipse\n"
"X-POFile-SpellExtra: Del Enter editing polylinetool Krita freehandpathtool\n"
"X-POFile-SpellExtra: toolbeziercurve options Shape example mouseleft tools\n"
"X-POFile-IgnoreConsistency: Type\n"

#: ../../<generated>:1
msgid "Close Ellipse"
msgstr "Elipse Fechada"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:12
msgid ""
".. image:: images/icons/shape_edit_tool.svg\n"
"   :alt: toolshapeedit"
msgstr ""
".. image:: images/icons/shape_edit_tool.svg\n"
"   :alt: ferramenta de edição de formas"

#: ../../<rst_epilog>:24
msgid ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: toolline"
msgstr ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: ferramenta da linha"

#: ../../<rst_epilog>:26
msgid ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: toolrectangle"
msgstr ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: ferramenta do rectângulo"

#: ../../<rst_epilog>:28
msgid ""
".. image:: images/icons/ellipse_tool.svg\n"
"   :alt: toolellipse"
msgstr ""
".. image:: images/icons/ellipse_tool.svg\n"
"   :alt: ferramenta da elipse"

#: ../../<rst_epilog>:30
msgid ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"
msgstr ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: ferramenta do polígono"

#: ../../<rst_epilog>:32
msgid ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: toolpolyline"
msgstr ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: ferramenta da linha poligonal"

#: ../../<rst_epilog>:34
msgid ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: toolbeziercurve"
msgstr ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: ferramenta de curva bézier"

#: ../../<rst_epilog>:36
msgid ""
".. image:: images/icons/freehand_path_tool.svg\n"
"   :alt: toolfreehandpath"
msgstr ""
".. image:: images/icons/freehand_path_tool.svg\n"
"   :alt: ferramenta de caminho livre"

#: ../../reference_manual/tools/shape_edit.rst:1
msgid "Krita's shape edit tool reference."
msgstr "A referência da ferramenta de edição de formas do Krita."

#: ../../reference_manual/tools/shape_edit.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/shape_edit.rst:11
msgid "Vector"
msgstr "Vector"

#: ../../reference_manual/tools/shape_edit.rst:11
msgid "Shape Edit"
msgstr "Edição de Formas"

#: ../../reference_manual/tools/shape_edit.rst:16
msgid "Shape Edit Tool"
msgstr "Ferramenta de Edição de Formas"

#: ../../reference_manual/tools/shape_edit.rst:18
msgid "|toolshapeedit|"
msgstr "|toolshapeedit|"

#: ../../reference_manual/tools/shape_edit.rst:20
msgid ""
"The shape editing tool is for editing vector shapes. In Krita versions "
"before 4.0 it would only show up in the docker when you had a vector shape "
"selected. In Krita 4.0, this tool is always visible and has the Shape "
"Properties docker as a part of it."
msgstr ""
"A ferramenta de edição de formas serve para editar formas vectoriais. Nas "
"versões do Krita anteriores à 4.0, só aparecia na área acoplável quando "
"tinha uma forma vectorial seleccionada. No Krita 4.0, esta ferramenta fica "
"sempre visível e tem a área de Propriedades da Forma como parte dela."

#: ../../reference_manual/tools/shape_edit.rst:23
msgid ".. image:: images/tools/Shape-editing-tool-example.png"
msgstr ".. image:: images/tools/Shape-editing-tool-example.png"

#: ../../reference_manual/tools/shape_edit.rst:24
msgid ""
"You can access the Edit Shapes tool by clicking on the icon in the toolbox, "
"but you can also access it by pressing the :kbd:`Enter` key when in the "
"Shape Selection tool and having a shape selected that can be most "
"efficiently edited with the edit shapes tool (right now, that's all shapes "
"but text)."
msgstr ""
"Poderá aceder à ferramenta de Edição de Formas se carregar no ícone da caixa "
"de ferramentas, mas poderá também aceder à mesma se carregar no :kbd:`Enter` "
"quando estiver na ferramenta de Selecção de Formas e tiver uma forma "
"seleccionada, a qual poderá ser editada de forma mais eficiente com a "
"ferramenta para editar formas (neste momento, são todas as formas menos o "
"texto)."

#: ../../reference_manual/tools/shape_edit.rst:27
msgid "On Canvas Editing of Shapes"
msgstr "Edição das Formas na Área de Desenho"

#: ../../reference_manual/tools/shape_edit.rst:29
msgid ""
"As detailed further in the Tool Options, there's a difference between path "
"shapes and specialized vector shapes that make it easy to have perfect "
"ellipses, rectangles and more."
msgstr ""
"Como será detalhado posteriormente nas Opções da Ferramenta, existe uma "
"diferença entre as formas de caminhos e as formas vectoriais especializadas "
"que tornam fácil a criação de elipses perfeitas, rectângulos, entre outros."

#: ../../reference_manual/tools/shape_edit.rst:32
#: ../../reference_manual/tools/shape_edit.rst:91
msgid "Path Shapes"
msgstr "Formas de Caminhos"

#: ../../reference_manual/tools/shape_edit.rst:34
msgid "Path shapes can be recognized by the different nodes they have."
msgstr ""
"As formas de caminhos podem ser reconhecidas de acordo com os nós diferentes "
"que têm."

#: ../../reference_manual/tools/shape_edit.rst:36
msgid ""
"Paths in Krita are mostly bezier curves, and are made up of nodes. For "
"straight lines, the nodes are connected by a line-segment and that's it. For "
"curved lines, each node has a side handle to allow curving of that segment "
"using the `cubic bezier curve algorithm <https://en.wikipedia.org/wiki/B"
"%C3%A9zier_curve#/media/File:B%C3%A9zier_3_big.gif>`_."
msgstr ""
"Os caminhos no Krita são na grande maioria curvas Bézier e são compostas por "
"nós. Para as linhas rectas, os nós estão ligados por um segmento de linha e "
"é tudo. Para as linhas curvas, cada nó tem uma pega lateral que permite "
"curvar esse segmento com o `algoritmo de curvas cúbicas de Bézier <https://"
"en.wikipedia.org/wiki/B%C3%A9zier_curve#/media/File:B%C3%A9zier_3_big.gif>`_."

#: ../../reference_manual/tools/shape_edit.rst:38
msgid ""
"**What that means, in short, is that moving the side handles into a given "
"direction will make the segment curve in that direction, and the longer the "
"line of the node to the side handle, the stronger the curving.**"
msgstr ""
"**O que isto significa, em resumo, é que se mover as pegas dos lados para "
"uma dada direcção, fará com que o segmento curve nessa direcção, e quanto "
"maior for a linha do nó até à pega do lado, mais forte será a curva.**"

#: ../../reference_manual/tools/shape_edit.rst:41
msgid "Selecting Nodes for Editing"
msgstr "Selecção dos Nós para Edição"

#: ../../reference_manual/tools/shape_edit.rst:43
msgid ""
"You can select a single node with |mouseleft|, they will turn bright green "
"if selected."
msgstr ""
"Poderá seleccionar um único nó com o |mouseleft|; o mesmo ficará verde claro "
"se estiver seleccionado."

#: ../../reference_manual/tools/shape_edit.rst:45
msgid ""
"|mouseleft| :kbd:`+ Shift` on unselected nodes will add them to a selection."
msgstr ""
"O |mouseleft| + :kbd:`Shift` sobre nós não seleccionados adicioná-los-á a "
"uma selecção."

#: ../../reference_manual/tools/shape_edit.rst:47
msgid ""
"|mouseleft| + drag will make a selection rectangle. All nodes whose handles "
"are touched by the rectangle will be selected. This combines with the |"
"mouseleft| :kbd:`+ Shift` shortcut above."
msgstr ""
"O |mouseleft| + arrastamento irá criar um rectângulo de selecção. Todos os "
"nós cujas pegas sejam tocadas pelo rectângulo ficarão seleccionadas. Isto "
"combina com o |mouseleft| + :kbd:`Shift` acima."

#: ../../reference_manual/tools/shape_edit.rst:50
msgid "Selected Nodes"
msgstr "Nós Seleccionados"

#: ../../reference_manual/tools/shape_edit.rst:52
msgid ""
"You can add and remove side handles from a selected node with the |"
"mouseleft| :kbd:`+ Shift` shortcut."
msgstr ""
"Poderá adicionar e remover as pegas laterais a partir de um nó seleccionado "
"com o |mouseleft| + :kbd:`Shift`."

#: ../../reference_manual/tools/shape_edit.rst:54
msgid ""
"Krita has several node-types that allow you control the side handles more "
"efficiently. These are the corner, smooth and symmetric modes."
msgstr ""
"O Krita tem diversos tipos de nós que lhe permitem controlar as pegas "
"laterais de forma mais eficiente. Estes são os nós de cantos, suaves e "
"simétricos."

#: ../../reference_manual/tools/shape_edit.rst:56
msgid "Corner"
msgstr "Canto"

#: ../../reference_manual/tools/shape_edit.rst:57
msgid ""
"Represented by a circle, the corner type allows you to have handles that can "
"point in different directions and have different lengths."
msgstr ""
"Representados por um círculo, os tipos do canto permitem-lhe ter pegas que "
"possam apontar em diferentes direcções e ter diferentes tamanhos."

#: ../../reference_manual/tools/shape_edit.rst:58
msgid "Smooth"
msgstr "Suave"

#: ../../reference_manual/tools/shape_edit.rst:59
msgid ""
"Represented by a square, the smooth type will ensure a smooth transition by "
"always pointing the handles into opposite directions, but they can still "
"have different lengths."
msgstr ""
"Representado por um quadrado, o tipo suave irá garantir uma transição suave, "
"apontando sempre as pegas em direcções opostas mas podendo ter à mesma "
"diferentes tamanhos."

#: ../../reference_manual/tools/shape_edit.rst:61
msgid "Symmetric"
msgstr "Simétricos"

#: ../../reference_manual/tools/shape_edit.rst:61
msgid ""
"Represented by a diamond, the symmetric node will force handles to always "
"point in opposite directions and have the same length."
msgstr ""
"Representado por um losango, o nó simétrico irá obrigar as pegas a apontar "
"sempre em direcções opostas e a ter o mesmo tamanho."

#: ../../reference_manual/tools/shape_edit.rst:63
msgid ""
"|mouseleft| :kbd:`+ Ctrl` on a selected node will cycle between the node-"
"types."
msgstr ""
"O |mouseleft| + :kbd:`Ctrl` sobre um nó seleccionado irá circular entre os "
"vários tipos de nós."

#: ../../reference_manual/tools/shape_edit.rst:65
msgid ":kbd:`Del` will remove the selected node."
msgstr "O :kbd:`Del` irá remover o nó seleccionado."

#: ../../reference_manual/tools/shape_edit.rst:68
msgid "Selected Segments"
msgstr "Segmentos Seleccionados"

#: ../../reference_manual/tools/shape_edit.rst:70
msgid ""
"Segments are the lines between nodes. Hovering over a segment will show a "
"dotted line, indicating it can be selected."
msgstr ""
"Os segmentos são as linhas entre nós. Se passar o cursor sobre um segmento, "
"irá mostrar uma linha com pontos, indicando que poderá ser seleccionada."

#: ../../reference_manual/tools/shape_edit.rst:72
msgid ""
"You can |mouseleft| and drag on a segment to curve it to the mouse point. "
"Clicking on different parts of the segment and dragging will curve it "
"differently."
msgstr ""
"Poderá usar o |mouseleft| e arrastar sobre um segmento para o curva para o "
"ponto do rato. Se carrega em diferentes partes do segmento e se o arrastar, "
"curvá-lo-á de forma diferente."

#: ../../reference_manual/tools/shape_edit.rst:74
msgid ""
"Double |mouseleft| on a segment will add a node on the segment under the "
"mouse cursor. The new node will be selected."
msgstr ""
"Faça um duplo-click com o |mouseleft| sobre um segmento irá adicionar um nó "
"sobre o segmento sob o cursor do rato. O novo nó ficará seleccionado."

#: ../../reference_manual/tools/shape_edit.rst:77
msgid "Other Shapes"
msgstr "Outras Formas"

#: ../../reference_manual/tools/shape_edit.rst:79
msgid ""
"Shapes that aren't path shapes only have a single type of node: A small "
"diamond like, that changes the specific parameters of that shape on-canvas. "
"For example, you can change the corner radius on rectangles by dragging the "
"nodes, or make the ellipse into a pie-segment."
msgstr ""
"As formas que não sejam formas de caminhos só tẽm um único tipo de nó: Um "
"pequeno losango que muda os parâmetros específicos dessa forma na área de "
"desenho. Por exemplo, poderá alterar o raio do canto nos rectângulos, caso "
"arraste os nós, ou poderá transformar a elipse num segmento circular em "
"fatia."

#: ../../reference_manual/tools/shape_edit.rst:82
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/shape_edit.rst:85
msgid ".. image:: images/tools/Shape-editing-tool-tool-options.png"
msgstr ".. image:: images/tools/Shape-editing-tool-tool-options.png"

#: ../../reference_manual/tools/shape_edit.rst:86
msgid ""
"Path shapes have options. The top left options are for converting to "
"different anchor point types. The bottom left options are for adding or "
"removing points. The top right options are for converting the line to "
"different types. The bottom right options are for breaking and joining line "
"segments."
msgstr ""
"As formas de caminhos têm opções. As opções do canto superior esquerdo "
"servem para converter para tipos diferentes de pontos de âncora. As opções "
"inferiores esquerdas servem para adicionar ou remover pontos. As opções "
"superiores direitas servem para converter a linha para diferentes tipos. As "
"opções inferiores direitas servem para quebrar e reunir segmentos da linha."

#: ../../reference_manual/tools/shape_edit.rst:88
msgid ""
"The tool options of the Edit Shapes Tool change depending on the type of "
"shape you have selected. With the exception of the path shape, all shapes "
"have a :guilabel:`Convert to Path` action, which converts said shape to a "
"path shape."
msgstr ""
"As opções da ferramenta para Editar Formas mudam de acordo com o tipo de "
"forma que seleccionou. Com a excepção da forma do caminho todas as formas "
"têm uma acção para :guilabel:`Converter para Caminho`, que converte a forma "
"indicada para uma forma de caminho."

#: ../../reference_manual/tools/shape_edit.rst:93
msgid ""
"|toolbeziercurve|, |toolline|, |toolpolyline|, |toolpolygon|, |"
"toolfreehandpath|"
msgstr ""
"|toolbeziercurve|, |toolline|, |toolpolyline|, |toolpolygon|, |"
"toolfreehandpath|"

#: ../../reference_manual/tools/shape_edit.rst:95
msgid ""
"Path shapes are the most common shape and can be made with the following "
"tools:"
msgstr ""
"As formas de caminhos são as formas mais comuns e podem ser criadas com as "
"seguintes ferramentas:"

#: ../../reference_manual/tools/shape_edit.rst:97
msgid ":ref:`path_tool`"
msgstr ":ref:`path_tool`"

#: ../../reference_manual/tools/shape_edit.rst:98
msgid ":ref:`line_tool`"
msgstr ":ref:`line_tool`"

#: ../../reference_manual/tools/shape_edit.rst:99
msgid ":ref:`polygon_tool`"
msgstr ":ref:`polygon_tool`"

#: ../../reference_manual/tools/shape_edit.rst:100
msgid ":ref:`polyline_tool`"
msgstr ":ref:`polyline_tool`"

#: ../../reference_manual/tools/shape_edit.rst:101
msgid ":ref:`freehand_path_tool`"
msgstr ":ref:`freehand_path_tool`"

#: ../../reference_manual/tools/shape_edit.rst:104
msgid "Edit the nodes."
msgstr "Edita os nós."

#: ../../reference_manual/tools/shape_edit.rst:106
msgid "Corner Point"
msgstr "Ponto do Canto"

#: ../../reference_manual/tools/shape_edit.rst:107
msgid ""
"Make the selected node a corner or cusp. This means that the side handles "
"can point in different directions and be different lengths."
msgstr ""
"Transforma o nó seleccionado num canto. Isto significa que as pegas laterais "
"poderão apontar em direcções diferentes e terem diferentes tamanhos."

#: ../../reference_manual/tools/shape_edit.rst:108
msgid "Smooth Point"
msgstr "Ponto Suave"

#: ../../reference_manual/tools/shape_edit.rst:109
msgid ""
"Make the selected node smooth. The two side handles will always point in "
"opposite directions, but their length can be different."
msgstr ""
"Transforma o nó seleccionado num suave. As duas pegas laterais irão sempre "
"apontar em direcções opostas, mas o seu tamanho poderá ser diferente."

#: ../../reference_manual/tools/shape_edit.rst:110
msgid "Symmetric Point"
msgstr "Ponto Simétrico"

#: ../../reference_manual/tools/shape_edit.rst:111
msgid ""
"Make the selected node smooth. The two side handles will always point in "
"opposite directions, and their length will stay the same."
msgstr ""
"Transforma o nó seleccionado num suave. As duas pegas laterais irão sempre "
"apontar em direcções opostas e o seu tamanho ficará o mesmo."

#: ../../reference_manual/tools/shape_edit.rst:112
msgid "Insert Point"
msgstr "Inserir um Ponto"

#: ../../reference_manual/tools/shape_edit.rst:113
msgid "Insert a new node into the middle of the selected segment."
msgstr "Insere um nó a meio do segmento seleccionado."

#: ../../reference_manual/tools/shape_edit.rst:115
msgid "Node Editing"
msgstr "Edição do Nó"

#: ../../reference_manual/tools/shape_edit.rst:115
msgid "Remove Point"
msgstr "Remover o Ponto"

#: ../../reference_manual/tools/shape_edit.rst:115
msgid "Remove the selected node."
msgstr "Remover o nó seleccionado."

#: ../../reference_manual/tools/shape_edit.rst:118
msgid "Edit line segments between nodes."
msgstr "Edita os segmentos da linha entre nós."

#: ../../reference_manual/tools/shape_edit.rst:120
msgid "Segment To Line"
msgstr "Segmento para Linha"

#: ../../reference_manual/tools/shape_edit.rst:121
msgid "Make the current segment a straight line."
msgstr "Transforma o segmento actual numa linha recta."

#: ../../reference_manual/tools/shape_edit.rst:122
msgid "Segment To Curve"
msgstr "Segmento para Curva"

#: ../../reference_manual/tools/shape_edit.rst:123
msgid ""
"Make the current segment a curve: It'll add side handles for this segment to "
"the nodes attached to it."
msgstr ""
"Transforma o segmento actual numa curva: Isto irá adicionar pegas laterais, "
"para este segmento, aos nós associados a ele."

#: ../../reference_manual/tools/shape_edit.rst:124
msgid "Make Line Point"
msgstr "Criar um Ponto da Linha"

#: ../../reference_manual/tools/shape_edit.rst:125
msgid ""
"Turn the selected node into a sharp corner: This will remove the side "
"handles."
msgstr ""
"Transforma o nó seleccionado num canto vincado: Isto irá remover as pegas "
"laterais."

#: ../../reference_manual/tools/shape_edit.rst:126
msgid "Make Curve Point"
msgstr "Criar um Ponto da Curva"

#: ../../reference_manual/tools/shape_edit.rst:127
msgid ""
"Turn the selected node into one that can curve: This will add side handles "
"to the node."
msgstr ""
"Transforma o nó seleccionado num nó que pode curvar: Isto irá adicionar "
"pegas laterais ao nó."

#: ../../reference_manual/tools/shape_edit.rst:128
msgid "Break at Point"
msgstr "Quebrar no Ponto"

#: ../../reference_manual/tools/shape_edit.rst:129
msgid "Break the path at this point."
msgstr "Quebra o caminho no ponto indicado em questão."

#: ../../reference_manual/tools/shape_edit.rst:130
msgid "Break Segment"
msgstr "Quebrar o Segmento"

#: ../../reference_manual/tools/shape_edit.rst:131
msgid "Break the path at the selected segment."
msgstr "Quebra o caminho no segmento seleccionado."

#: ../../reference_manual/tools/shape_edit.rst:132
msgid "Join with Segment"
msgstr "Juntar ao Segmento"

#: ../../reference_manual/tools/shape_edit.rst:133
msgid "Join two nodes that are only attached on one side with a segment."
msgstr "Junta dois nós que só estão associados de um lado com um segmento."

#: ../../reference_manual/tools/shape_edit.rst:135
msgid "Line Segment Editing"
msgstr "Edição de Segmentos da Linha"

#: ../../reference_manual/tools/shape_edit.rst:135
msgid "Merge Points"
msgstr "Reunir os Pontos"

#: ../../reference_manual/tools/shape_edit.rst:135
msgid ""
"Merge two nodes into one, if the nodes are adjacent or if both nodes are "
"only attached on one side with a segment."
msgstr ""
"Junta dois nós num só, se os nós forem adjacentes ou se ambos os nós só "
"estiverem associados de um lado a um segmento."

#: ../../reference_manual/tools/shape_edit.rst:138
msgid "Rectangle Shapes"
msgstr "Formas Rectangulares"

#: ../../reference_manual/tools/shape_edit.rst:140
msgid "|toolrectangle|"
msgstr "|toolrectangle|"

#: ../../reference_manual/tools/shape_edit.rst:142
msgid ""
"Rectangle shapes are the ones made with the :ref:`rectangle_tool`. It has "
"extra options to make rounded corners easy."
msgstr ""
"As formas rectangulares são as que são criadas com a :ref:`rectangle_tool`. "
"Tem opções extra que facilitam a criação de cantos arredondados."

#: ../../reference_manual/tools/shape_edit.rst:144
msgid "Corner radius x"
msgstr "Raio em X do canto"

#: ../../reference_manual/tools/shape_edit.rst:145
msgid "The radius of the x-axis of the corner curve."
msgstr "O raio do eixo dos X da curva do canto."

#: ../../reference_manual/tools/shape_edit.rst:147
msgid "Corner radius y"
msgstr "Raio em Y do canto"

#: ../../reference_manual/tools/shape_edit.rst:147
msgid "The radius of the y-axis of the corner curve."
msgstr "O raio do eixo dos Y da curva do canto."

#: ../../reference_manual/tools/shape_edit.rst:150
msgid "Ellipse Shapes"
msgstr "Formas Elípticas"

#: ../../reference_manual/tools/shape_edit.rst:152
msgid "|toolellipse|"
msgstr "|toolellipse|"

#: ../../reference_manual/tools/shape_edit.rst:154
msgid "Ellipse shapes are the ones made with the :ref:`ellipse_tool`."
msgstr "As formas elípticas são as que são criadas com a :ref:`ellipse_tool`."

#: ../../reference_manual/tools/shape_edit.rst:157
msgid "The type of ellipse shape it is."
msgstr "O tipo de forma elíptica que é."

#: ../../reference_manual/tools/shape_edit.rst:159
msgid "Arc"
msgstr "Arco"

#: ../../reference_manual/tools/shape_edit.rst:160
msgid "An arc shape will keep the path open when it isn't fully circular."
msgstr ""
"Uma forma em arco irá manter o caminho aberto quando não for completamente "
"circular."

#: ../../reference_manual/tools/shape_edit.rst:161
msgid "Pie"
msgstr "Circular"

#: ../../reference_manual/tools/shape_edit.rst:162
msgid ""
"A pie shape will add two extra lines to the center when the shape isn't "
"fully circular, like how one cuts out a piece from a pie."
msgstr ""
"Uma forma em fatia circular irá adicionar duas linhas até ao centro quando "
"não for completamente circular, como se alguém cortasse uma fatia de tarte."

#: ../../reference_manual/tools/shape_edit.rst:164
msgid "Type"
msgstr "Tipo"

#: ../../reference_manual/tools/shape_edit.rst:164
msgid "Cord"
msgstr "Corda"

#: ../../reference_manual/tools/shape_edit.rst:164
msgid ""
"A cord shape will add a straight line between the two ends if the path isn't "
"fully circular, as if a cord is being strung between the two points."
msgstr ""
"Uma forma em corda irá adicionar uma linha recta entre os dois extremos do "
"caminho, caso não seja completamente circular, como se tivesse estendido uma "
"corda entre os dois pontos."

#: ../../reference_manual/tools/shape_edit.rst:166
msgid "Start Angle"
msgstr "Ângulo Inicial"

#: ../../reference_manual/tools/shape_edit.rst:167
msgid "The angle at which the shape starts."
msgstr "O ângulo no qual começa a forma."

#: ../../reference_manual/tools/shape_edit.rst:168
msgid "End Angle"
msgstr "Ângulo Final"

#: ../../reference_manual/tools/shape_edit.rst:169
msgid "The angle at which the shape ends."
msgstr "O ângulo no qual termina a forma."

#: ../../reference_manual/tools/shape_edit.rst:171
msgid "An action to quickly make the ellipse fully circular."
msgstr "Uma acção para tornar rapidamente a elipse num círculo completo."
