# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:16+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: BlendingmodesMultiplyLightblueandOrange\n"
"X-POFile-SpellExtra: BlendingmodesMultiplySampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesAdditionRedplusgray\n"
"X-POFile-SpellExtra: BlendingmodesDivideSampleimagewithdots Krita image\n"
"X-POFile-SpellExtra: BlendingmodesAdditionLightblueandOrange\n"
"X-POFile-SpellExtra: BlendingmodesInverseSubtractLightblueandOrange\n"
"X-POFile-SpellExtra: BlendingmodesSubtractLightblueandOrange blendingmodes\n"
"X-POFile-SpellExtra: BlendingmodesSubtractSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesAdditionSampleimagewithdots images\n"
"X-POFile-SpellExtra: BlendingmodesInverseSubtractSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDivideLightblueandOrange arithmetic\n"

#: ../../reference_manual/blending_modes/arithmetic.rst:1
msgid ""
"Page about the arithmetic blending modes in Krita: Addition, Divide, Inverse "
"Subtract, Multiply and Subtract."
msgstr ""
"Página sobre os modos de mistura aritmética no Krita: Adição, Divisão, "
"Subtracção Inversa, Multiplicação e Subtracção."

#: ../../reference_manual/blending_modes/arithmetic.rst:14
msgid "Arithmetic"
msgstr "Matemática"

#: ../../reference_manual/blending_modes/arithmetic.rst:16
msgid "These blending modes are based on simple maths."
msgstr "Estes modos matemáticos são baseados em contas matemáticas simples."

#: ../../reference_manual/blending_modes/arithmetic.rst:18
msgid "Addition (Blending Mode)"
msgstr "Adição (Modo de Mistura)"

#: ../../reference_manual/blending_modes/arithmetic.rst:22
msgid "Addition"
msgstr "Adição"

#: ../../reference_manual/blending_modes/arithmetic.rst:24
msgid "Adds the numerical values of two colors together:"
msgstr "Adição dos valores numéricos de duas cores em conjunto:"

#: ../../reference_manual/blending_modes/arithmetic.rst:26
msgid "Yellow(1, 1, 0) + Blue(0, 0, 1) = White(1, 1, 1)"
msgstr "Amarelo(1, 1, 0) + Azul(0, 0, 1) = Branco(1, 1, 1)"

#: ../../reference_manual/blending_modes/arithmetic.rst:28
msgid ""
"Darker Gray(0.4, 0.4, 0.4) + Lighter Gray(0.5, 0.5, 0.5) = Even Lighter Gray "
"(0.9, 0.9, 0.9)"
msgstr ""
"Cinzento Escuro(0,4, 0,4, 0,4) + Cinzento Claro(0,5, 0,5, 0,5) = Cinzento "
"Mais Claro (0,9, 0,9, 0,9)"

#: ../../reference_manual/blending_modes/arithmetic.rst:33
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:33
#: ../../reference_manual/blending_modes/arithmetic.rst:40
#: ../../reference_manual/blending_modes/arithmetic.rst:47
#: ../../reference_manual/blending_modes/arithmetic.rst:54
msgid "Left: **Normal**. Right: **Addition**."
msgstr "Esquerda: **Normal**. Direita: **Adição**."

#: ../../reference_manual/blending_modes/arithmetic.rst:35
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) + Orange(1, 0.5961, 0.0706) = (1.1608, "
"1.2235, 0.8980) → Very Light Yellow(1, 1, 0.8980)"
msgstr ""
"Azul Claro(0,1608, 0,6274, 0,8274) + Laranja(1, 0,5961, 0,0706) = (1,1608, "
"1,2235, 0,8980) → Amarelo Muito Claro(1, 1, 0,8980)"

#: ../../reference_manual/blending_modes/arithmetic.rst:40
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:42
msgid "Red(1, 0, 0) + Gray(0.5, 0.5, 0.5) = Pink(1, 0.5, 0.5)"
msgstr "Vermelho(1, 0, 0) + Cinzento(0,5, 0,5, 0,5) = Rosa(1, 0,5, 0,5)"

#: ../../reference_manual/blending_modes/arithmetic.rst:47
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Red_plus_gray.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Red_plus_gray.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:49
msgid ""
"When the result of the addition is more than 1, white is the color "
"displayed. Therefore, white plus any other color results in white. On the "
"other hand, black plus any other color results in the added color."
msgstr ""
"Quando o resultado da soma é maior que 1, será apresentado o branco. Como "
"tal, o branco mais qualquer cor irá resultar em branco. Por outro lado, se "
"adicionar o preto a qualquer cor, irá obter essa cor adicionada."

#: ../../reference_manual/blending_modes/arithmetic.rst:54
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:59
msgid "Divide"
msgstr "Divisão"

#: ../../reference_manual/blending_modes/arithmetic.rst:61
msgid "Divides the numerical value from the lower color by the upper color."
msgstr "Divide o valor numérico da cor inferior pela cor superior."

#: ../../reference_manual/blending_modes/arithmetic.rst:63
msgid "Red(1, 0, 0) / Gray(0.5, 0.5, 0.5) = (2, 0, 0) → Red(1, 0, 0)"
msgstr ""
"Vermelho(1, 0, 0) / Cinzento(0,5, 0,5, 0,5) = (2, 0, 0) → Vermelho(1, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:65
msgid ""
"Darker Gray(0.4, 0.4, 0.4) / Lighter Gray(0.5, 0.5, 0.5) = Even Lighter Gray "
"(0.8, 0.8, 0.8)"
msgstr ""
"Cinzento Escuro(0,4, 0,4, 0,4) / Cinzento Claro(0,5, 0,5, 0,5) = Cinzento "
"Mais Claro(0,8, 0,8, 0,8)"

#: ../../reference_manual/blending_modes/arithmetic.rst:70
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:70
#: ../../reference_manual/blending_modes/arithmetic.rst:77
#: ../../reference_manual/blending_modes/arithmetic.rst:82
msgid "Left: **Normal**. Right: **Divide**."
msgstr "Esquerda: **Normal**. Direita: **Divisão**."

#: ../../reference_manual/blending_modes/arithmetic.rst:72
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) / Orange(1, 0.5961, 0.0706) = (0.1608, "
"1.0525, 11.7195) → Aqua(0.1608, 1, 1)"
msgstr ""
"Azul Claro(0,1608, 0,6274, 0,8274) / Laranja(1, 0,5961, 0,0706) = (0,1608, "
"1,0525, 11,7195) → Água(0,1608, 1, 1)"

#: ../../reference_manual/blending_modes/arithmetic.rst:77
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:82
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:87
msgid "Inverse Subtract"
msgstr "Subtrair (Inverso)"

#: ../../reference_manual/blending_modes/arithmetic.rst:89
msgid ""
"This inverts the lower layer before subtracting it from the upper layer."
msgstr "Isto inverte a camada inferior antes de a subtrair da camada superior."

#: ../../reference_manual/blending_modes/arithmetic.rst:91
msgid ""
"Lighter Gray(0.5, 0.5, 0.5)_(1_Darker Gray(0.4, 0.4, 0.4)) = (-0.1, -0.1, "
"-0.1) → Black(0, 0, 0)"
msgstr ""
"Cinzento Claro(0,5, 0,5, 0,5)_(1_Cinzento Escuro(0,4, 0,4, 0,4)) = (-0,1, "
"-0,1, -0,1) → Preto(0, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:96
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:96
#: ../../reference_manual/blending_modes/arithmetic.rst:103
#: ../../reference_manual/blending_modes/arithmetic.rst:108
msgid "Left: **Normal**. Right: **Inverse Subtract**."
msgstr "Esquerda: **Normal**. Direita: **Subtracção Inversa**."

#: ../../reference_manual/blending_modes/arithmetic.rst:98
msgid ""
"Orange(1, 0.5961, 0.0706)_(1_Light Blue(0.1608, 0.6274, 0.8274)) = (0.1608, "
"0.2235, -0.102) → Dark Green(0.1608, 0.2235, 0)"
msgstr ""
"Laranja(1, 0,5961, 0,0706)_(1_Azul Claro(0,1608, 0,6274, 0,8274)) = (0,1608, "
"0,2235, -0,102) → Verde Escuro(0,1608, 0,2235, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:103
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:108
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:113
msgid "Multiply"
msgstr "Multiplicação"

#: ../../reference_manual/blending_modes/arithmetic.rst:115
msgid ""
"Multiplies the two colors with each other, but does not go beyond the upper "
"limit."
msgstr ""
"Multiplica as duas cores uma pela outra, mas não ultrapassa o limite "
"superior."

#: ../../reference_manual/blending_modes/arithmetic.rst:117
msgid ""
"This is often used to color in a black and white lineart. One puts the black "
"and white lineart on top, and sets the layer to 'Multiply', and then draw in "
"color on a layer beneath. Multiply will all the color to go through."
msgstr ""
"Isto é normalmente usado para aplicar cores sobre imagens a preto-e-branco. "
"É colocada a imagem a preto-e-branco no topo e é definida a camada como "
"'Multiplicação', sendo depois desenhado a cores numa camada abaixo. A "
"multiplicação irá permitir que todas as cores passam através dela."

#: ../../reference_manual/blending_modes/arithmetic.rst:120
msgid "White(1,1,1) x White(1, 1, 1) = White(1, 1, 1)"
msgstr "Branco(1,1,1) x Branco(1, 1, 1) = Branco(1, 1, 1)"

#: ../../reference_manual/blending_modes/arithmetic.rst:122
msgid "White(1, 1, 1) x Gray(0.5, 0.5, 0.5) = Gray(0.5, 0.5, 0.5)"
msgstr "Branco(1, 1, 1) x Cinzento(0,5, 0,5, 0,5) = Cinzento(0,5, 0,5, 0,5)"

#: ../../reference_manual/blending_modes/arithmetic.rst:124
msgid ""
"Darker Gray(0.4, 0.4, 0.4) x Lighter Gray(0.5, 0.5, 0.5) = Even Darker Gray "
"(0.2, 0.2, 0.2)"
msgstr ""
"Cinzento Escuro(0,4, 0,4, 0,4) x Cinzento Claro(0,5, 0,5, 0,5) = Cinzento "
"Mais Escuro(0,2, 0,2, 0,2)"

#: ../../reference_manual/blending_modes/arithmetic.rst:129
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:129
#: ../../reference_manual/blending_modes/arithmetic.rst:136
#: ../../reference_manual/blending_modes/arithmetic.rst:141
msgid "Left: **Normal**. Right: **Multiply**."
msgstr "Esquerda: **Normal**. Direita: **Multiplicação**."

#: ../../reference_manual/blending_modes/arithmetic.rst:131
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) x Orange(1, 0.5961, 0.0706) = "
"Green(0.1608, 0.3740, 0.0584)"
msgstr ""
"Azul Claro(0,1608, 0,6274, 0,8274) x Laranja(1, 0,5961, 0,0706) = "
"Verde(0,1608, 0,3740, 0,0584)"

#: ../../reference_manual/blending_modes/arithmetic.rst:136
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:141
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:146
msgid "Subtract"
msgstr "Subtracção"

#: ../../reference_manual/blending_modes/arithmetic.rst:148
msgid "Subtracts the top layer from the bottom layer."
msgstr "Subtrai a camada de cima da camada de baixo."

#: ../../reference_manual/blending_modes/arithmetic.rst:150
msgid "White(1, 1, 1)_White(1, 1, 1) = Black(0, 0, 0)"
msgstr "Branco(1, 1, 1)_Branco(1, 1, 1) = Preto(0, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:152
msgid "White(1, 1, 1)_Gray(0.5, 0.5, 0.5) = Gray(0.5, 0.5, 0.5)"
msgstr "Branco(1, 1, 1)_Cinzento(0,5, 0,5, 0,5) = Cinzento(0,5, 0,5, 0,5)"

#: ../../reference_manual/blending_modes/arithmetic.rst:154
msgid ""
"Darker Gray(0.4, 0.4, 0.4)_Lighter Gray(0.5, 0.5, 0.5) = (-0.1, -0.1, -0.1) "
"→ Black(0, 0, 0)"
msgstr ""
"Cinzento Escuro(0,4, 0,4, 0,4)_Cinzento Claro(0,5, 0,5, 0,5) = (-0,1, -0,1, "
"-0,1) → Preto(0, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:159
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:159
#: ../../reference_manual/blending_modes/arithmetic.rst:166
#: ../../reference_manual/blending_modes/arithmetic.rst:171
msgid "Left: **Normal**. Right: **Subtract**."
msgstr "Esquerda: **Normal**. Direita: **Subtracção**."

#: ../../reference_manual/blending_modes/arithmetic.rst:161
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) - Orange(1, 0.5961, 0.0706) = (-0.8392, "
"0.0313, 0.7568) → Blue(0, 0.0313, 0.7568)"
msgstr ""
"Azul Claro(0,1608, 0,6274, 0,8274) - Laranja(1, 0,5961, 0,0706) = (-0,8392, "
"0,0313, 0,7568) → Azul(0, 0,0313, 0,7568)"

#: ../../reference_manual/blending_modes/arithmetic.rst:166
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:171
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Sample_image_with_dots.png"
