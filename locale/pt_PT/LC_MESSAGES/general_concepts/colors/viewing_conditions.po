# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-20 00:58+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en LUT girassóis image metamerismo\n"
"X-POFile-SpellExtra: Kritaexamplemetamerism images papoilas Shirt shirt\n"
"X-POFile-SpellExtra: Vermeer MIC cLUT ICC Commons\n"
"X-POFile-SpellExtra: Kritametamerismpresentation Meisjemetdeparelviewing\n"
"X-POFile-SpellExtra: colorcategory Whitepointmixupex Krita Maurits OCIO\n"
"X-POFile-SpellExtra: Metamerismo\n"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_example_metamerism.png"
msgstr ".. image:: images/color_category/Krita_example_metamerism.png"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_01.svg"
msgstr ".. image:: images/color_category/White_point_mix_up_ex1_01.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mixup_ex1_02.png"
msgstr ".. image:: images/color_category/White_point_mixup_ex1_02.png"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_03.svg"
msgstr ".. image:: images/color_category/White_point_mix_up_ex1_03.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_metamerism_presentation.svg"
msgstr ".. image:: images/color_category/Krita_metamerism_presentation.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:1
msgid "What are viewing conditions."
msgstr "O que são as condições de visualização."

#: ../../general_concepts/colors/viewing_conditions.rst:10
#: ../../general_concepts/colors/viewing_conditions.rst:15
msgid "Viewing Conditions"
msgstr "Condições de Visualização"

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Metamerism"
msgstr "Metamerismo"

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Color"
msgstr "Cor"

#: ../../general_concepts/colors/viewing_conditions.rst:17
msgid ""
"We mentioned viewing conditions before, but what does this have to do with "
"'white points'?"
msgstr ""
"Mencionámos as condições de visualização anteriormente, mas o que é que isto "
"tem a ver com os 'pontos brancos'?"

#: ../../general_concepts/colors/viewing_conditions.rst:19
msgid ""
"A lot actually, rather, white points describe a type of viewing condition."
msgstr ""
"Bastante, de facto. Os pontos brancos descrevem um dado tipo de condição de "
"visualização."

#: ../../general_concepts/colors/viewing_conditions.rst:21
msgid ""
"So, usually what we mean by viewing conditions is the lighting and "
"decoration of the room that you are viewing the image in. Our eyes try to "
"make sense of both the colors that you are looking at actively (the colors "
"of the image) and the colors you aren't looking at actively (the colors of "
"the room), which means that both sets of colors affect how the image looks."
msgstr ""
"Como tal, o que normalmente chamados de condições de visualização é a "
"iluminação e a decoração do quarto no qual está a ver a imagem. Os nossos "
"olhos tentam tirar o sentido de ambas as cores que está a ver de forma "
"activa (as cores da imagem) e das cores que não está a ver activamente (as "
"cores da sala), o que significa que ambos os conjuntos de cores afectam a "
"aparência da imagem."

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ".. image:: images/color_category/Meisje_met_de_parel_viewing.png"
msgstr ".. image:: images/color_category/Meisje_met_de_parel_viewing.png"

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ""
"**Left**: Let's ruin Vermeer by putting a bright purple background that asks "
"for more attention than the famous painting itself. **Center**: a much more "
"neutral backdrop that an interior decorator would hate but brings out the "
"colors. **Right**: The approximate color that this painting is displayed "
"against in real life in the Maurits House, at the least, last time I was "
"there. Original image from wikipedia commons."
msgstr ""
"**Esquerda**: Vamos arruinar um Vermeer, colocando um fundo roxo brilhante "
"que chama mais a atenção que a famosa pintura em si. **Centro**: uma versão "
"muito mais neutra que um decorador de interiores iria detestar mas que "
"realça as cores. **Direita**: A cor aproximada com que esta pintura é "
"apresentada na vida real na Casa de Maurits, na última vez que lá estive. A "
"imagem original do Wikipédia Commons."

#: ../../general_concepts/colors/viewing_conditions.rst:29
msgid ""
"This is for example, the reason why museum exhibitioners can get really "
"angry at the interior decorators when the walls of the museum are painted "
"bright red or blue, because this will drastically change the way how the "
"painting's colors look. (Which, if we are talking about a painter known for "
"their colors like Vermeer, could result in a really bad experience)."
msgstr ""
"Isto é, por exemplo, a razão pela qual os criadores de exibições em museus "
"podem ficar realmente irritados com os decoradores de interiores, quando as "
"paredes do museu são pintadas com vermelho ou azul claro, porque essas cores "
"irão mudar drasticamente a forma como as cores da pintura aparecem. (O que, "
"se estivermos a falar sobre um pintor conhecido pelas suas cores, como era "
"Vermeer, poderá resultar numa experiência realmente desagradável)."

#: ../../general_concepts/colors/viewing_conditions.rst:37
msgid ""
"Lighting is the other component of the viewing condition which can have "
"dramatic effects. Lighting in particular affects the way how all colors "
"look. For example, if you were to paint an image of sunflowers and poppies, "
"print that out, and shine a bright yellow light on it, the sunflowers would "
"become indistinguishable from the white background, and the poppies would "
"look orange. This is called `metamerism <https://en.wikipedia.org/wiki/"
"Metamerism_%28color%29>`_, and it's generally something you want to avoid in "
"your color management pipeline."
msgstr ""
"A iluminação é o outro componente da condição de visualização que poderá "
"provocar efeitos dramáticos. A iluminação em particular afecta a forma como "
"todas as cores aparecem. Por exemplo, se fosse para pintar uma imagem com "
"girassóis e papoilas, imprimi-las e aplicasse uma luz amarela clara sobre "
"ela, os girassóis quase ficavam escondidos no meio do fundo branco, e as "
"papoilas iriam ficar alaranjadas. Este fenómeno chama-se `metamerismo "
"<https://en.wikipedia.org/wiki/Metamerism_%28color%29>`, e normalmente é "
"algo que quer evitar no seu processo de gestão de cores."

#: ../../general_concepts/colors/viewing_conditions.rst:39
msgid ""
"An example where metamerism could become a problem is when you start "
"matching colors from different sources together."
msgstr ""
"Os exemplos onde o metamerismo se pode tornar um problema é quando começar a "
"corresponder cores de diferentes origens em conjunto."

#: ../../general_concepts/colors/viewing_conditions.rst:46
msgid ""
"For example, if you are designing a print for a red t-shirt that's not "
"bright red, but not super grayish red either. And you want to make sure the "
"colors of the print match the color of the t-shirt, so you make a dummy "
"background layer that is approximately that red, as correctly as you can "
"observe it, and paint on layers above that dummy layer. When you are done, "
"you hide this dummy layer and sent the image with a transparent background "
"to the press."
msgstr ""
"Por exemplo, se estiver a desenhar uma impressão para uma T-Shirt vermelha, "
"que não é vermelha-clara, mas também não é um vermelho acinzentado. E você "
"quer ter a certeza que as cores da impressão correspondem à cor da T-shirt, "
"pelo que poderá criar um fundo de exemplo que tenha esse vermelho, mais ou "
"menos tão correcto quando consegue observar, e pinte as camadas por cima da "
"camada de testes. Quando terminar, poderá esconder essa camada de testes e "
"enviar a imagem com um fundo transparente para a casa de impressão."

#: ../../general_concepts/colors/viewing_conditions.rst:54
msgid ""
"But when you get the t-shirt from the printer, you notice that all your "
"colors look off, mismatched, and maybe too yellowish (and when did that T-"
"Shirt become purple?)."
msgstr ""
"Mas quando recebe a T-Shirt da impressora, irá reparar que todas as cores "
"parecem desfasadas, sem correspondência e talvez um pouco amareladas (e "
"desde quando a T-Shirt ficou roxa?)."

#: ../../general_concepts/colors/viewing_conditions.rst:56
msgid "This is where white points come in."
msgstr "Isto é onde entram os pontos brancos."

#: ../../general_concepts/colors/viewing_conditions.rst:58
msgid ""
"You probably observed the t-shirt in a white room where there were "
"incandescent lamps shining, because as a true artist, you started your work "
"in the middle of the night, as that is when the best art is made. However, "
"incandescent lamps have a black body temperature of roughly 2300-2800K, "
"which makes them give a yellowish light, officially called White Point A."
msgstr ""
"Provavelmente observou a T-shirt numa sala branca onde existiam lâmpadas "
"incandescentes a brilhar, já que como um verdadeiro artista, começou o seu "
"trabalho a meio da noite, já que é a melhor hora em que as melhores obras "
"são feitas. Contudo, as luzes incandescentes têm uma temperatura corporal de "
"aproximadamente 2300-2800K, o que lhes atribui uma luz amarelada, sendo "
"chamada oficialmente de Ponto Branco A."

#: ../../general_concepts/colors/viewing_conditions.rst:61
msgid ""
"Your computer screen on the other hand, has a black body temperature of "
"6500K, also known as D65. Which is a far more blueish color of light than "
"the lamps you are hanging."
msgstr ""
"O ecrã do seu computador, por outro lado, tem uma temperatura corporal do "
"preto equivalente a 6500K, também conhecida por D65. O que corresponde a uma "
"cor mais azulada da luz que as lâmpadas que possui."

#: ../../general_concepts/colors/viewing_conditions.rst:63
msgid ""
"What's worse, Printers print on the basis of using a white point of D50, the "
"color of white paper under direct sunlight."
msgstr ""
"O que é pior ainda, as impressoras imprimem na base de usar um ponto branco "
"D50, a cor do papel branco face à luz do Sol directa."

#: ../../general_concepts/colors/viewing_conditions.rst:70
msgid ""
"So, by eye-balling your t-shirt's color during the evening, you took its red "
"color as transformed by the yellowish light. Had you made your observation "
"in diffuse sunlight of an overcast (which is also roughly D65), or made it "
"in direct sunlight light and painted your picture with a profile set to D50, "
"the color would have been much closer, and thus your design would not be as "
"yellowish."
msgstr ""
"Por isso, por observar a cor da sua T-shirt durante a noite, você observou a "
"sua cor vermelha transformada pela luz amarelada. Se tivesse feito a sua "
"observação à luz do Sol difusa numa tempestade (que também é aproximada à "
"D65), ou se a tivesse feito à luz do Sol directa e tivesse pintado a sua "
"imagem com um perfil associado ao D50, a cor teria sido muito mais próxima "
"e, como tal, o seu desenho não teria saído tão amarelado."

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ".. image:: images/color_category/White_point_mixup_ex1_03.png"
msgstr ".. image:: images/color_category/White_point_mixup_ex1_03.png"

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ""
"Applying a white balance filter will sort of match the colors to the tone as "
"in the middle, but you would have had a much better design had you designed "
"against the actual color to begin with."
msgstr ""
"A aplicação de um filtro de balanceamento de branco irá de certa forma "
"corresponder as cores à tonalidade como acontece no meio, mas iria ter um "
"desenho muito melhor se tivesse desenhado com a cor actual, em primeiro "
"lugar."

#: ../../general_concepts/colors/viewing_conditions.rst:79
msgid ""
"Now, you could technically quickly fix this by using a white balancing "
"filter, like the ones in G'MIC, but because this error is caught at the end "
"of the production process, you basically limited your use of possible colors "
"when you were designing, which is a pity."
msgstr ""
"Agora, poderia tentar corrigir isto de forma técnica, usando um filtro de "
"balanceamento de brancos, como os existentes no G'MIC, mas dado que este "
"erro só é detectado no fim do processo de produção, acabou por limitar o seu "
"uso de cores possíveis quando esteve a desenhar, o que é pena."

#: ../../general_concepts/colors/viewing_conditions.rst:81
msgid ""
"Another example where metamerism messes things up is with screen projections."
msgstr ""
"Outro exemplo onde aparece o metamerismo a prejudicar é com os projectores "
"de ecrãs."

#: ../../general_concepts/colors/viewing_conditions.rst:83
msgid ""
"We have a presentation where we mark one type of item with red, another with "
"yellow and yet another with purple. On a computer the differences between "
"the colors are very obvious."
msgstr ""
"Temos uma apresentação onde marcamos um tipo de item a vermelho, outro a "
"amarelo e outro a roxo. Num computador, as diferenças entre as cores são "
"bastante óbvias."

#: ../../general_concepts/colors/viewing_conditions.rst:89
msgid ""
"However, when we start projecting, the lights of the room aren't dimmed, "
"which means that the tone scale of the colors becomes crunched, and yellow "
"becomes near indistinguishable from white. Furthermore, because the light in "
"the room is slightly yellowish, the purple is transformed into red, making "
"it indistinguishable from the red. Meaning that the graphic is difficult to "
"read."
msgstr ""
"Contudo, quando começamos a projectar, as luzes da sala não estão "
"escurecidas, o que significa que a escala de tonalidades das cores fica "
"afectada, o que significa que o amarelo fica quase igual ao branco. Para "
"além disso, dado que a luz na sala é ligeiramente amarelada, o roxo fica "
"transformado em vermelho, tornando-o quase igual ao vermelho. Finalmente, "
"isto significa que a imagem é difícil de ler."

#: ../../general_concepts/colors/viewing_conditions.rst:91
msgid ""
"In both cases, you can use Krita's color management a little to help you, "
"but mostly, you just need to be ''aware'' of it, as Krita can hardly fix "
"that you are looking at colors at night, or the fact that the presentation "
"hall owner refuses to turn off the lights."
msgstr ""
"Em ambos os casos, poderá usar a gestão de cores do Krita para o ajudar um "
"pouco, mas na maior parte dos casos, só precisa de estar ''a par'' disso, "
"dado que o Krita dificilmente consegue corrigir o que está à procura nas "
"cores à noite, ou o facto de o dono da sala onde decorrerá a apresentação se "
"recusar a desligar as luzes."

#: ../../general_concepts/colors/viewing_conditions.rst:93
msgid ""
"That said, unless you have a display profile that uses LUTs, such as an OCIO "
"LUT or a cLUT icc profile, white point won't matter much when choosing a "
"working space, due to weirdness in the icc v4 workflow which always converts "
"matrix profiles with relative colorimetric, meaning the white points are "
"matched up."
msgstr ""
"Posto isto, a menos que tenha um perfil de visualização que use LUT's, como "
"o do OCIO ou um perfil ICC do cLUT, o ponto branco não será muito relevante "
"quando escolher um espaço de cores de trabalho, devido à estranheza do "
"funcionamento do ICC v4, que converte sempre os perfis em matriz com a "
"tentativa colorimétrica relativa, o que significa que os pontos brancos têm "
"correspondência."
