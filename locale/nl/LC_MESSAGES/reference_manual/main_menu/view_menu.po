# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-06-15 23:04+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../<generated>:1
msgid "Show Painting Previews"
msgstr "Voorbeelden van tekeningen tonen"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: muisrechts"

#: ../../reference_manual/main_menu/view_menu.rst:1
msgid "The view menu in Krita."
msgstr "Het menu Beeld in Krita."

#: ../../reference_manual/main_menu/view_menu.rst:11
msgid "View"
msgstr "Beeld"

#: ../../reference_manual/main_menu/view_menu.rst:11
msgid "Wrap around mode"
msgstr "Modus regels-afbreken"

#: ../../reference_manual/main_menu/view_menu.rst:16
msgid "View Menu"
msgstr "Menu Beeld"

#: ../../reference_manual/main_menu/view_menu.rst:18
msgid "Show Canvas Only"
msgstr "Alleen werkveld tonen"

#: ../../reference_manual/main_menu/view_menu.rst:19
msgid ""
"Only shows the canvas and what you have configured to show in :guilabel:"
"`Canvas Only` settings."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:20
msgid "Fullscreen mode"
msgstr "Volledig scherm"

#: ../../reference_manual/main_menu/view_menu.rst:21
msgid "This will hide the system bar."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:22
msgid "Wrap Around Mode"
msgstr "Modus regels-afbreken"

#: ../../reference_manual/main_menu/view_menu.rst:23
msgid ""
"This will show the image as if tiled orthographically. Very useful for "
"tiling 3d textures. Hit the :kbd:`W` key to quickly activate it."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:24
msgid "Instant Preview"
msgstr "Instant vooruitblik"

#: ../../reference_manual/main_menu/view_menu.rst:25
msgid "Toggle :ref:`instant_preview` globally."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:26
msgid "Soft Proofing"
msgstr "Soft-proofing"

#: ../../reference_manual/main_menu/view_menu.rst:27
msgid "Activate :ref:`soft_proofing`."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:28
msgid "Out of Gamut Warnings"
msgstr "Waarschuwing buiten gamut"

#: ../../reference_manual/main_menu/view_menu.rst:29
msgid "See the :ref:`soft_proofing` page for details."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:30
msgid "Canvas"
msgstr "Werkveld"

#: ../../reference_manual/main_menu/view_menu.rst:31
msgid "Contains view manipulation actions."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:32
msgid "Mirror View"
msgstr "Beeld spiegelen"

#: ../../reference_manual/main_menu/view_menu.rst:33
msgid ""
"This will mirror the view. Hit the :kbd:`M` key to quickly activate it. Very "
"useful during painting."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:34
msgid "Show Rulers"
msgstr "Linialen tonen"

#: ../../reference_manual/main_menu/view_menu.rst:35
msgid ""
"This will display a set of rulers. |mouseright| the rulers after showing "
"them, to change the units."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:36
msgid "Rulers track pointer"
msgstr "Trackaanwijzer van liniaal"

#: ../../reference_manual/main_menu/view_menu.rst:37
msgid ""
"This adds a little marker to the ruler to show where the mouse is in "
"relation to them."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:38
msgid "Show Guides"
msgstr "Hulplijnen tonen"

#: ../../reference_manual/main_menu/view_menu.rst:39
msgid "Show or hide the guides."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:40
msgid "Lock Guides"
msgstr "Hulplijnen vergrendelen"

#: ../../reference_manual/main_menu/view_menu.rst:41
msgid "Prevent the guides from being able to be moved by the cursor."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:42
msgid "Show Status Bar"
msgstr "Statusbalk tonen"

#: ../../reference_manual/main_menu/view_menu.rst:43
msgid ""
"This will show the status bar. The status bar contains a lot of important "
"information, a zoom widget, and the button to switch Selection Display Mode."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:44
msgid "Show Grid"
msgstr "Raster tonen"

#: ../../reference_manual/main_menu/view_menu.rst:45
msgid "Shows and hides the grid. Shortcut: :kbd:`Ctrl + Shift + '`"
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:46
msgid "Show Pixel Grid"
msgstr "Pixelraster tonen"

#: ../../reference_manual/main_menu/view_menu.rst:47
msgid "Show the pixel grid as configured in the :ref:`display_settings`."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:48
msgid "Snapping"
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:49
msgid "Toggle the :ref:`snapping` types."
msgstr ""

#: ../../reference_manual/main_menu/view_menu.rst:50
msgid "Show Painting Assistants"
msgstr "Tekenassistenten tonen"

#: ../../reference_manual/main_menu/view_menu.rst:51
msgid "Shows or hides the Assistants."
msgstr "Toont of verbergt de assistenten"

#: ../../reference_manual/main_menu/view_menu.rst:53
msgid "Shows or hides the Previews."
msgstr "Toont of verbergt de voorbeelden."
