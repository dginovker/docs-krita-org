# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-06 11:40+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:1
msgid "How to use layer styles in Krita."
msgstr "Hoe laagstijlen in Krita gebruiken."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
#: ../../reference_manual/layers_and_masks/layer_styles.rst:17
msgid "Layer Styles"
msgstr "Laagstijlen"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "Layer Effects"
msgstr "Laageffecten"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "Layer FX"
msgstr "Laag-FX"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "ASL"
msgstr "ASL"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:19
msgid ""
"Layer styles are effects that are added on top of your layer. They are "
"editable and can easily be toggled on and off. To add a layer style to a "
"layer go to :menuselection:`Layer --> Layer Style`. You can also right-click "
"a layer to access the layer styles."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:22
msgid ""
"When you have the layer styles window up, make sure that the :guilabel:"
"`Enable Effects` item is checked."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:24
msgid ""
"There are a variety of effects and styles you can apply to a layer. When you "
"add a style, your layer docker will show an extra \"Fx\" icon. This allows "
"you to toggle the layer style effects on and off."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:30
msgid ""
"This feature was added to increase support for :program:`Adobe Photoshop`. "
"The features that are included mirror what that application supports."
msgstr ""
